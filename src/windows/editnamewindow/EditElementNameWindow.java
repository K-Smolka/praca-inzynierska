package windows.editnamewindow;

import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import treeviewcustom.treeitems.AbstractTreeItem;

import java.io.IOException;

public class EditElementNameWindow {
	private Parent root;
	private Stage stage;

	public EditElementNameWindow(AbstractTreeItem treeItem) {
		
        try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("EditElementNameWindowController.fxml"));
			loader.setController(new EditElementNameWindowController(treeItem));

			root = loader.load();
			stage = new Stage();
			stage.setOnCloseRequest(event -> treeItem.closeEditWindow());
			stage.setTitle("Edit name");
			stage.setResizable(false);
			stage.setScene(new Scene(root));
        }
        catch (IOException e) {
            e.printStackTrace();
        }
	}
	
	public Parent getRoot(){
		return root;
	}
	public void show(){
		stage.show();
	}
}
