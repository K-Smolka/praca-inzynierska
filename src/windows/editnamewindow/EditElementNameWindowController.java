package windows.editnamewindow;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import treeviewcustom.treeitems.AbstractTreeItem;
import utility.TextFieldValidable;
import validation.ValidableData;
import validation.ValidationData;
import validation.Validator;
import windows.alertwindow.AlertWindow;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Krzysztof S. on 2017-07-11.
 */
class EditElementNameWindowController implements Initializable {

	@FXML
	HBox mainLayout;

	@FXML
	TextFieldValidable textFieldNewName;

	@FXML
	Button buttonOK;

	AbstractTreeItem treeItem;

	/**
	 *
	 * @param treeItem
	 * TreeItem dla ktorego zostalo wywolane to okno.
	 */
	public EditElementNameWindowController(AbstractTreeItem treeItem) {
		this.treeItem = treeItem;
	}

	@FXML
	public void changeName(){
		if(!validateNameTextField())
			return;

		treeItem.editName();
		closeWindow();
	}

	private void closeWindow(){
		Stage stage = (Stage)mainLayout.getScene().getWindow();
		stage.fireEvent(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		textFieldNewName.addValidation(new ValidationData(Validator.ValidationTypes.IS_NOT_EMPTY, null));
		textFieldNewName.textProperty().addListener((observable, oldValue, newValue) -> {
			textFieldNewName.setValidationStyle(true);
		});
	}

	private boolean validateNameTextField() {
		for (ValidationData validation: textFieldNewName.getValidations()){
			ValidableData validableData = new ValidableData(textFieldNewName.getText(), validation);
			if(!Validator.validate(validableData).hasValidationPassed()){
				textFieldNewName.setValidationStyle(false);
				return false;
			}
		}
		return true;
	}
}
