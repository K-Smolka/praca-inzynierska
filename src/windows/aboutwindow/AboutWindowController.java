package windows.aboutwindow;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import utility.GlobalProperties;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Krzysztof S. on 2017-10-01.
 */
public class AboutWindowController implements Initializable {
	@FXML
	Label labelVersionNumber;

	@FXML
	Button buttonOK;

	@FXML
	private void close(){
		((Stage)buttonOK.getScene().getWindow()).close();
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		labelVersionNumber.setText(GlobalProperties.VERSION + "");
	}
}
