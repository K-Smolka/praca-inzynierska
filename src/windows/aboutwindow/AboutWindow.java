package windows.aboutwindow;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import windows.alertwindow.AlertWindow;

import java.io.IOException;

/**
 * Created by Krzysztof S. on 2017-10-01.
 */
public class AboutWindow {
	private Parent root;
	private Stage stage;

	public AboutWindow() {

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("AboutWindowView.fxml"));
			loader.setController(new AboutWindowController());

			root = loader.load();
			stage = new Stage();
			stage.setTitle("About application");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setResizable(false);
			stage.setScene(new Scene(root));
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Parent getRoot(){
		return root;
	}
	public void show(){
		stage.show();
	}

}
