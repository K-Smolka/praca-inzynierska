package windows.addsegmentwindow;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import windows.alertwindow.AlertWindow;

import java.io.IOException;

/**
 * Created by Krzysztof S. on 2017-08-31.
 */
public class AddSegmentWindow {
	private Parent root;
	private Stage stage;

	public AddSegmentWindow(int idOfTheCallingNode) {

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("AddSegmentWindowController.fxml"));
			loader.setController(new AddSegmentWindowController(idOfTheCallingNode));

			root = loader.load();
			stage = new Stage();
			stage.setTitle("Add new segment");
			stage.setResizable(false);
			stage.setScene(new Scene(root));
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Parent getRoot(){
		return root;
	}
	public void show(){
		stage.show();
	}
}
