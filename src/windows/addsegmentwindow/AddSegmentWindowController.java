package windows.addsegmentwindow;

import application.FileElementsContainer;
import elements.Element;
import elements.FileSegment;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import utility.TextFieldValidable;
import validation.ValidableData;
import validation.ValidationData;
import validation.Validator;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Krzysztof S. on 2017-08-31.
 */
public class AddSegmentWindowController implements Initializable{

	@FXML
	HBox mainLayout;

	@FXML
	TextFieldValidable nameTextField;

	@FXML
	Button addSegmentButton;

	int idOfTheCallingNode;

	public AddSegmentWindowController(int idOfTheCallingNode) {
		this.idOfTheCallingNode = idOfTheCallingNode;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		nameTextField.addValidation(new ValidationData(Validator.ValidationTypes.IS_NOT_EMPTY, null));
		nameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
			nameTextField.setValidationStyle(true);
		});
	}

	@FXML
	private void addSegmentButtonAction(){
		if(!validateNameTextField())
			return;

		addNewSegmentToFile(idOfTheCallingNode, nameTextField.getText());
		closeWindow();
	}

	private void addNewSegmentToFile(int idOfTheCallingNode, String name){
		FileSegment segment = new FileSegment(name);
		Element parentFileElement = FileElementsContainer.searchElementById(idOfTheCallingNode);
		parentFileElement.addComponent(segment);
		parentFileElement.getTreeItemHandler().setExpanded(true);
	}

	private boolean validateNameTextField() {
		for (ValidationData validation: nameTextField.getValidations()){
			ValidableData validableData = new ValidableData(nameTextField.getText(), validation);
			if(!Validator.validate(validableData).hasValidationPassed()){
				nameTextField.setValidationStyle(false);
				return false;
			}
		}
		return true;
	}

	void closeWindow(){
		Stage stage = (Stage)mainLayout.getScene().getWindow();
		stage.fireEvent(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));
	}
}
