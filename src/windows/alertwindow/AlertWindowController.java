package windows.alertwindow;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Krzysztof S. on 2017-08-21.
 */
class AlertWindowController implements Initializable {

	@FXML
	TextArea textAreaAlertMessage;

	@FXML
	Button buttonOK;

	private String alertMessage;

	public AlertWindowController(String alertMessage) {
		this.alertMessage = alertMessage;
	}

	@FXML
	public void close(){
		((Stage)buttonOK.getScene().getWindow()).close();
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		textAreaAlertMessage.setText(alertMessage);
	}
}
