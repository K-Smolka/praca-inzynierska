package windows.alertwindow;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by Krzysztof S. on 2017-08-21.
 */
public class AlertWindow {
	private Parent root;
	private Stage stage;

	public AlertWindow(String alertMessage) {

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("AlertWindowController.fxml"));
			loader.setController(new AlertWindowController(alertMessage));

			root = loader.load();
			stage = new Stage();
			stage.setTitle("Error!");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setResizable(false);
			stage.setScene(new Scene(root));
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Parent getRoot(){
		return root;
	}
	public void show(){
		stage.show();
	}

}
