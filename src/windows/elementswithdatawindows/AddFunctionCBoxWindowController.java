package windows.elementswithdatawindows;

import elements.ElementTag;
import elements.dataelements.*;
import elements.dataelements.simpledataelements.ElementWithSimpleData;
import elements.dataelements.simpledataelements.Point2d;
import elements.dataelements.simpledataelements.Point3d;
import elements.dataelements.simpledataelements.StringVariable;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import utility.TextFieldValidable;
import validation.ValidationData;
import validation.Validator;
import windows.alertwindow.AlertWindow;

import java.io.IOException;
import java.net.URL;
import java.util.*;

import static windows.elementswithdatawindows.AddElementModel.createStringVariable;

/**
 * Created by Krzysztof S. on 2017-08-28.
 */
public class AddFunctionCBoxWindowController implements Initializable {
	@FXML
	HBox mainLayout;

	@FXML
	ListView<String> functionsListView;

	@FXML
	private Button removeFunctionButton;

	@FXML
	private ListView<ElementWithData> selectedFunctionElementsListView;

	@FXML
	private Button removeFunctionElementButton;

	@FXML
	private TextFieldValidable functionNameTextField;

	@FXML
	private Button addNewFunctionButton;

	@FXML
	private ComboBox<ElementTag> addDataTypeComboBox;

	@FXML
	private Button addDataTypeToFunctionButton;

	@FXML
	CheckBox addToAllRowsCheckBox;

	@FXML
	Button createButton;

	int idOfTheCallingNode;

	// StringBuilder is used because of that, it is not immutable, and can be used as a key multiple times in Map.
	Map<String, List<ElementWithData>> functionsMap;

	List<Stage> childWindows;

	public AddFunctionCBoxWindowController(int idOfTheCallingNode ) {
		this.idOfTheCallingNode = idOfTheCallingNode;
		this.functionsMap = new HashMap<>();
		this.childWindows = new ArrayList<>();
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		ObservableList<ElementTag> creatableDataTypes = FXCollections.observableList(new ArrayList<>(Arrays.asList(ElementTag.values())));
		creatableDataTypes.remove(ElementTag.FILE);
		creatableDataTypes.remove(ElementTag.SEGMENT);
		creatableDataTypes.remove(ElementTag.ROW);
		creatableDataTypes.remove(ElementTag.FUNCTION);
		addDataTypeComboBox.setItems(creatableDataTypes);
		addDataTypeComboBox.getSelectionModel().select(0);

		functionsListView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				selectedFunctionElementsListView.getItems().clear();
				if(newValue != null)
					for(ElementWithData functionElement: functionsMap.get(newValue)){
						selectedFunctionElementsListView.getItems().add(functionElement);
					}
			}
		});

		functionsListView.setCellFactory(TextFieldListCell.forListView());
		functionsListView.setEditable(true);
		functionsListView.setOnEditCommit(t -> {
			String oldFunctionName = functionsListView.getItems().get(t.getIndex());
			functionsMap.put(t.getNewValue(), functionsMap.get(oldFunctionName));
			functionsMap.remove(oldFunctionName);
			functionsListView.getItems().set(t.getIndex(), t.getNewValue());
		});

 		selectedFunctionElementsListView.setCellFactory(lv -> new ListCell<ElementWithData>() {
			@Override
			public void updateItem(ElementWithData item, boolean empty) {
				super.updateItem(item, empty);
				if (empty) {
					setText(null);
				} else {
					if(item.getName() != null && !"".equals(item.getName()))
						setText(item.getName() + " - " + item.elementTag.toString());
					else
						setText(item.elementTag.toString());
				}
			}
		});

		functionNameTextField.addValidation(new ValidationData(Validator.ValidationTypes.IS_NOT_EMPTY, null));
		functionNameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
			functionNameTextField.setValidationStyle(true);
		});

		afterInitialization();
	}

	void afterInitialization(){}

	@FXML
	private void addNewFunctionButtonAction(){
		String newFunctionName = functionNameTextField.getText();

		if (!validateFunctionName(newFunctionName))
			return;

		functionsListView.getItems().add(newFunctionName);
		functionsMap.put(newFunctionName, new ArrayList<>());

		functionsListView.getSelectionModel().select(newFunctionName);
		functionNameTextField.setText("");
	}

	private boolean validateFunctionName(String nameTextFiledContent) {
		if(!AddElementModel.validateTextField(functionNameTextField))
			return false;

		for(String functionName: functionsListView.getItems()) {
			if(functionName.equals(nameTextFiledContent)){
				new AlertWindow("There is already function with name like this!").show();
				return false;
			}
		}
		return true;
	}

	@FXML
	private void addDataTypeToFunctionButtonAction(){
		if(functionsListView.getSelectionModel().getSelectedItem() == null) {
			new AlertWindow("There is no function selected!").show();
			return;
		}

		try {
			FXMLLoader loader;
			loader = getFXMLLoaderOfSelectedDataType();

			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setTitle("Adding " + addDataTypeComboBox.getSelectionModel().getSelectedItem());
			stage.setScene(new Scene(root));

			functionsListView.setEditable(false);
			stage.setOnCloseRequest((event) -> {
				functionsListView.setEditable(true);
			});

			childWindows.add(stage);
			stage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private FXMLLoader getFXMLLoaderOfSelectedDataType() throws IOException{
		FXMLLoader loader;
		String currentlySelectedFunction = functionsListView.getSelectionModel().getSelectedItem();

		switch(addDataTypeComboBox.getSelectionModel().getSelectedItem()){
			case NUMERIC_VALUE:
				loader = new FXMLLoader(getClass().getResource("AddSimpleDataTypeWindowController.fxml"));
				loader.setController(new AddNumericValueWindowController(idOfTheCallingNode){
					@Override
					void afterInitialization(){
						setupDefaultValidations();
						addToAllRowsCheckBox.setDisable(true);
					}
					@Override
					void createButtonAction(){
						if(!AddElementModel.validateTextField(nameTextField) || !validateParameters())
							return;

						List<ValidationData> validations = new ArrayList<>(addedValidationsList.getItems());
						ElementWithData elementToAdd = AddElementModel.createNumericValue(nameTextField.getText(), validations);

						AddFunctionCBoxWindowController.this.addNewDataType(currentlySelectedFunction, elementToAdd);
						super.closeWindow();
					}
				});
				return loader;

			case POINT2D:
				loader = new FXMLLoader(getClass().getResource("AddPoint2DWindowController.fxml"));
				loader.setController(new AddPoint2DWindowController(idOfTheCallingNode){
					@Override
					void afterInitialization(){
						setupDefaultValidations();
						addToAllRowsCheckBox.setDisable(true);
					}
					@Override
					void createButtonAction(){
						if(!AddElementModel.validateTextField(nameTextField) || !validateParameters())
							return;

						Point2d elementToAdd = createElement();
						AddFunctionCBoxWindowController.this.addNewDataType(currentlySelectedFunction, elementToAdd);
						super.closeWindow();
					}
				});
				return loader;

			case POINT3D:
				loader = new FXMLLoader(getClass().getResource("AddPoint3DWindowController.fxml"));
				loader.setController(new AddPoint3DWindowController(idOfTheCallingNode){
					@Override
					void afterInitialization(){
						setupDefaultValidations();
						addToAllRowsCheckBox.setDisable(true);
					}
					@Override
					void createButtonAction(){
						if(!AddElementModel.validateTextField(nameTextField) || !validateParameters())
							return;

						Point3d elementToAdd = createElement();
						AddFunctionCBoxWindowController.this.addNewDataType(currentlySelectedFunction, elementToAdd);
						super.closeWindow();
					}
				});
				return loader;

			case STRING_VARIABLE:
				loader = new FXMLLoader(getClass().getResource("AddSimpleDataTypeWindowController.fxml"));
				loader.setController(new AddStringVariableWindowController(idOfTheCallingNode){
					@Override
					void afterInitialization(){
						addToAllRowsCheckBox.setDisable(true);
					}
					@Override
					void createButtonAction(){
						if(!AddElementModel.validateTextField(nameTextField) || !validateParameters())
							return;

						List<ValidationData> validations = new ArrayList<>(addedValidationsList.getItems());
						StringVariable elementToAdd = createStringVariable(nameTextField.getText(), validations);

						AddFunctionCBoxWindowController.this.addNewDataType(currentlySelectedFunction, elementToAdd);
						super.closeWindow();
					}
				});
				return loader;

			case GROUP:
				loader = new FXMLLoader(getClass().getResource("AddGroupWindowController.fxml"));
				loader.setController(new AddGroupWindowController(idOfTheCallingNode){
					@Override
					void afterInitialization(){
						addToAllRowsCheckBox.setDisable(true);
					}

					@Override
					void createAndAddGroup(ElementWithSimpleData pattern){
						Group elementToAdd = createGroup(pattern);
						AddFunctionCBoxWindowController.this.addNewDataType(currentlySelectedFunction, elementToAdd);
						super.closeWindow();
					}
				});
				return loader;

			case CONST_VALUES_CBOX:
				loader = new FXMLLoader(getClass().getResource("AddContValuesCBoxWindowController.fxml"));
				loader.setController(new AddContValuesCBoxWindowController(idOfTheCallingNode){
					@Override
					void afterInitialization(){
						addToAllRowsCheckBox.setDisable(true);
					}
					@Override
					void createButtonAction(){
						if(!AddElementModel.validateTextField(nameTextField) || !AddElementModel.checkListViewContainsAnyElementAndShowAlert(addedConstValuesList, new AlertWindow("Const values list can't be empty!")))
							return;

						List<String> constValues = new ArrayList<>(addedConstValuesList.getItems());
						ElementWithData elementToAdd = new ConstValuesCBox(nameTextField.getText(), constValues);
						AddFunctionCBoxWindowController.this.addNewDataType(currentlySelectedFunction, elementToAdd);
						super.closeWindow();
					}
				});
				return loader;

			case FUNCTION_CBOX:
				loader = new FXMLLoader(getClass().getResource("AddFunctionCBoxWindowController.fxml"));
				loader.setController(new AddFunctionCBoxWindowController(idOfTheCallingNode){
					@Override
					void afterInitialization(){
						addToAllRowsCheckBox.setDisable(true);
					}
					@Override
					void createButtonAction(){
						List<Function> functions = new ArrayList<>();

						for (Map.Entry<String, List<ElementWithData>> functionMapEntry :functionsMap.entrySet()) {
							functions.add(new Function(functionMapEntry.getKey(), functionMapEntry.getValue()));
						}

						AddFunctionCBoxWindowController.this.addNewDataType(currentlySelectedFunction, new FunctionCBox(functions));
						super.closeWindow();
					}
				});
				return loader;

			default:
				throw new IllegalArgumentException("Not supported type of DataType in AddFunctionCBoxWindowController.");
		}
	}

	void elementAddedAction(){
		functionsListView.setEditable(true);
	}

	private void addNewDataType(String function, ElementWithData elementToAdd){
		selectedFunctionElementsListView.getItems().add(elementToAdd);
		functionsMap.get(function).add(elementToAdd);
	}

	@FXML
	private void removeFunctionButtonAction(){
		if(functionsListView.getSelectionModel().getSelectedItem() != null){
			String functionToRemove = functionsListView.getSelectionModel().getSelectedItem();
			functionsListView.getItems().remove(functionToRemove);
			functionsMap.remove(functionToRemove);
		}
	}

	@FXML
	private void removeFunctionElementButtonAction(){
		if(selectedFunctionElementsListView.getSelectionModel().getSelectedItem() != null){
			String selectedFunction = functionsListView.getSelectionModel().getSelectedItem();
			ElementWithData elementToRemove = selectedFunctionElementsListView.getSelectionModel().getSelectedItem();
			selectedFunctionElementsListView.getItems().remove(elementToRemove);
			functionsMap.get(selectedFunction).remove(elementToRemove);
		}
	}

	@FXML
	void createButtonAction(){
		List<Function> functions = new ArrayList<>();

		for (Map.Entry<String, List<ElementWithData>> functionMapEntry :functionsMap.entrySet()) {
			functions.add(new Function(functionMapEntry.getKey(), functionMapEntry.getValue()));
		}

		if(!validateFunctionsListIsEmpty())
			return;

		AddElementModel.addFunctionCBoxToNode(idOfTheCallingNode, new FunctionCBox(functions), addToAllRowsCheckBox.isSelected());
		closeWindow();
	}


	boolean validateFunctionsListIsEmpty(){
		if(functionsMap.keySet().isEmpty()){
			new AlertWindow("At least 1 function is needed to create FUNCTION_CBOX!").show();
			return false;
		}
		return true;
	}

	protected void closeWindow(){
		for(Stage window: childWindows){
			window.fireEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSE_REQUEST));
		}
		Stage stage = (Stage)mainLayout.getScene().getWindow();
		stage.fireEvent(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));
	}
}
