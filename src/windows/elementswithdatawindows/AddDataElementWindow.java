package windows.elementswithdatawindows;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import windows.alertwindow.AlertWindow;

import java.io.IOException;
import java.util.Arrays;

/**
 * Created by Krzysztof S. on 2017-08-24.
 */
public class AddDataElementWindow {
	private Parent root;
	private Stage stage;

	public AddDataElementWindow(int idOfTheCallingNode)
	{
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("AddDataElementWindowController.fxml"));
			loader.setController(new AddDataElementWindowController(idOfTheCallingNode));

			root = loader.load();
			stage = new Stage();
			stage.setTitle("Adding new element");
			stage.setResizable(false);
			stage.setScene(new Scene(root));

			//For tests. It will catch every exception to prevent application crash.
			Thread.UncaughtExceptionHandler h = (th, ex) -> new AlertWindow(
					Arrays.stream(ex.getStackTrace()).
							map(stackTraceElement -> stackTraceElement.toString()).
							reduce((s, s2) -> s + System.lineSeparator() + s2).toString()).show();
			Thread.currentThread().setUncaughtExceptionHandler(h);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Parent getRoot(){
		return root;
	}
	public void show(){
		stage.show();
	}
}
