package windows.elementswithdatawindows;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListView;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import utility.TextFieldValidable;
import validation.ValidationData;
import validation.Validator;
import windows.alertwindow.AlertWindow;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by Krzysztof S. on 2017-08-24.
 */
public class AddContValuesCBoxWindowController implements Initializable {

	@FXML
	VBox mainLayout;

	@FXML
	TextFieldValidable nameTextField;

	@FXML
	TextFieldValidable newConstValueTextField;

	@FXML
	ListView<String> addedConstValuesList;

	@FXML
	CheckBox addToAllRowsCheckBox;

	@FXML
	Button createButton;

	int idOfTheCallingNode;

	public AddContValuesCBoxWindowController(int idOfTheCallingNode) {
		this.idOfTheCallingNode = idOfTheCallingNode;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		addedConstValuesList.setCellFactory(TextFieldListCell.forListView());
		addedConstValuesList.setOnEditCommit(t -> {
			addedConstValuesList.getItems().set(t.getIndex(), t.getNewValue());
		});

		nameTextField.addValidation(new ValidationData(Validator.ValidationTypes.IS_NOT_EMPTY, null));
		nameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
			nameTextField.setValidationStyle(true);
		});

		newConstValueTextField.addValidation(new ValidationData(Validator.ValidationTypes.IS_NOT_EMPTY, null));
		newConstValueTextField.textProperty().addListener((observable, oldValue, newValue) -> {
			newConstValueTextField.setValidationStyle(true);
		});

		afterInitialization();
	}

	void afterInitialization(){}

	@FXML
	private void addConstValueButtonAction(){
		if(!AddElementModel.validateTextField(newConstValueTextField) || !validateConstValuesListContainsDuplicateOf(newConstValueTextField.getText()))
			return;

		String newConstValue = newConstValueTextField.getText();

		if(!"".equals(newConstValue)){
			addedConstValuesList.getItems().add(newConstValue);
			newConstValueTextField.setText("");
		}
	}

	@FXML
	private void removaButtonAction(){
		addedConstValuesList.getItems().remove(addedConstValuesList.getSelectionModel().getSelectedIndex());
	}

	@FXML
	void createButtonAction(){
		if(!AddElementModel.validateTextField(nameTextField) || !validateConstValuesListIsEmpty())
			return;

		List<String> constValues = new ArrayList<>(addedConstValuesList.getItems());
		AddElementModel.addConstValuesCBoxToNode(idOfTheCallingNode, nameTextField.getText(), constValues, addToAllRowsCheckBox.isSelected());

		closeWindow();
	}

	boolean validateConstValuesListIsEmpty(){
		if(addedConstValuesList.getItems().isEmpty()){
			new AlertWindow("Const values list can't be empty!").show();
			return false;
		}
		return true;
	}

	boolean validateConstValuesListContainsDuplicateOf(String possibleDuplicate){
		if(addedConstValuesList.getItems().contains(possibleDuplicate)){
			new AlertWindow("Const values can't be duplicated!").show();
			return false;
		}
		return true;
	}

	void closeWindow(){
		Stage stage = (Stage)mainLayout.getScene().getWindow();
		stage.fireEvent(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));
	}
}
