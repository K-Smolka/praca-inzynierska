package windows.elementswithdatawindows;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by Krzysztof S. on 2017-09-18.
 */
public class EditDataElementWindow {
	private Parent root;
	private Stage stage;

	public EditDataElementWindow(int idOfTheCallingNode)
	{
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("EditDataElementWindowController.fxml"));
			loader.setController(new EditDataElementWindowController(idOfTheCallingNode));

			root = loader.load();
			stage = new Stage();
			stage.setTitle("Edit element");
			stage.setResizable(false);
			stage.setScene(new Scene(root));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Parent getRoot(){
		return root;
	}
	public void show(){
		stage.show();
	}
}
