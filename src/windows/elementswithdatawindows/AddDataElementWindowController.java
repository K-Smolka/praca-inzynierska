package windows.elementswithdatawindows;

import elements.ElementTag;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.ResourceBundle;

/**
 * Created by Krzysztof S. on 2017-08-22.
 */
public class AddDataElementWindowController implements Initializable {
	@FXML
	ListView listOfCreatableDataTypes;

	@FXML
	VBox workspace;

	HashMap<ElementTag, Parent> dataTypesControllers;

	int idOfTheCallingNode;

	public AddDataElementWindowController(int idOfTheCallingNode) throws IOException{
		this.idOfTheCallingNode = idOfTheCallingNode;
		dataTypesControllers = new HashMap();

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("AddSimpleDataTypeWindowController.fxml"));
		fxmlLoader.setController(new AddNumericValueWindowController(idOfTheCallingNode));
		dataTypesControllers.put(ElementTag.NUMERIC_VALUE, fxmlLoader.load());

		fxmlLoader = new FXMLLoader(getClass().getResource("AddSimpleDataTypeWindowController.fxml"));
		fxmlLoader.setController(new AddStringVariableWindowController(idOfTheCallingNode));
		dataTypesControllers.put(ElementTag.STRING_VARIABLE, fxmlLoader.load());

		fxmlLoader = new FXMLLoader(getClass().getResource("AddPoint2DWindowController.fxml"));
		fxmlLoader.setController(new AddPoint2DWindowController(idOfTheCallingNode));
		dataTypesControllers.put(ElementTag.POINT2D, fxmlLoader.load());

		fxmlLoader = new FXMLLoader(getClass().getResource("AddPoint3DWindowController.fxml"));
		fxmlLoader.setController(new AddPoint3DWindowController(idOfTheCallingNode));
		dataTypesControllers.put(ElementTag.POINT3D, fxmlLoader.load());

		fxmlLoader = new FXMLLoader(getClass().getResource("AddContValuesCBoxWindowController.fxml"));
		fxmlLoader.setController(new AddContValuesCBoxWindowController(idOfTheCallingNode));
		dataTypesControllers.put(ElementTag.CONST_VALUES_CBOX, fxmlLoader.load());

		fxmlLoader = new FXMLLoader(getClass().getResource("AddGroupWindowController.fxml"));
		fxmlLoader.setController(new AddGroupWindowController(idOfTheCallingNode));
		dataTypesControllers.put(ElementTag.GROUP, fxmlLoader.load());

		fxmlLoader = new FXMLLoader(getClass().getResource("AddFunctionCBoxWindowController.fxml"));
		fxmlLoader.setController(new AddFunctionCBoxWindowController(idOfTheCallingNode));
		dataTypesControllers.put(ElementTag.FUNCTION_CBOX, fxmlLoader.load());
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		ObservableList<ElementTag> creatableDataTypes = FXCollections.observableList(new ArrayList<>(Arrays.asList(ElementTag.values())));
		creatableDataTypes.remove(ElementTag.FILE);
		creatableDataTypes.remove(ElementTag.SEGMENT);
		creatableDataTypes.remove(ElementTag.ROW);
		creatableDataTypes.remove(ElementTag.FUNCTION);

		listOfCreatableDataTypes.setItems(creatableDataTypes);
		listOfCreatableDataTypes.getSelectionModel().selectedItemProperty().addListener(
			(ChangeListener<ElementTag>) (observable, oldValue, newValue) -> {
				workspace.getChildren().clear();
				workspace.getChildren().add(dataTypesControllers.get(newValue));
		});
	}
}
