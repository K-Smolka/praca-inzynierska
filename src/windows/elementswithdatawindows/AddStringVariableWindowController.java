package windows.elementswithdatawindows;

import elements.dataelements.simpledataelements.StringVariable;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import utility.TextFieldValidable;
import validation.ValidationData;
import validation.Validator;
import windows.alertwindow.AlertWindow;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by Krzysztof S. on 2017-08-24.
 */
public class AddStringVariableWindowController implements Initializable {
	@FXML
	ComboBox<Validator.ValidationTypes> validationsCBox;

	@FXML
	ListView<ValidationData> addedValidationsList;

	@FXML
	private ListView<String> validationParamsList;

	@FXML
	TextFieldValidable nameTextField;

	@FXML
	CheckBox addToAllRowsCheckBox;

	@FXML
	Button createButton;

	@FXML
	VBox mainLayout;

	int idOfTheCallingNode;

	public AddStringVariableWindowController(int idOfTheCallingNode) {
		this.idOfTheCallingNode = idOfTheCallingNode;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		ObservableList<Validator.ValidationTypes> validationTypesList = FXCollections.observableList(new ArrayList<>(Arrays.asList(Validator.ValidationTypes.values())));
		validationsCBox.setItems(validationTypesList);
		validationsCBox.getSelectionModel().select(0);

		addedValidationsList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<ValidationData>() {
			@Override
			public void changed(ObservableValue<? extends ValidationData> observable, ValidationData oldValue, ValidationData newValue) {
				validationParamsList.getItems().clear();
				if(!addedValidationsList.getItems().isEmpty())
					validationParamsList.getItems().addAll(addedValidationsList.getSelectionModel().getSelectedItem().validationParameters);
			}
		});

		validationParamsList.setCellFactory(TextFieldListCell.forListView());
		validationParamsList.setOnEditCommit(t -> {
			validationParamsList.getItems().set(t.getIndex(), t.getNewValue());
			addedValidationsList.getSelectionModel().getSelectedItem().validationParameters.set(t.getIndex(), t.getNewValue());
		});

		nameTextField.addValidation(new ValidationData(Validator.ValidationTypes.IS_NOT_EMPTY, null));
		nameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
			nameTextField.setValidationStyle(true);
		});

		afterInitialization();
	}

	void afterInitialization(){}

	@FXML
	private void addValidationButtonAction(){
		Validator.ValidationTypes selectedValidationInCBox = validationsCBox.getSelectionModel().getSelectedItem();
		if(selectedValidationInCBox != null){
			ArrayList<String> params = new ArrayList<>();
			for (int paramCounter=0; paramCounter < selectedValidationInCBox.getNumberOfParameters(); paramCounter++) {
				params.add("Parameter to change");
			}
			addedValidationsList.getItems().add(new ValidationData(selectedValidationInCBox, params));
		}
	}

	@FXML
	private void removaButtonAction(){
		addedValidationsList.getItems().remove(addedValidationsList.getSelectionModel().getSelectedItem());
	}

	@FXML
	void createButtonAction(){
		if(!AddElementModel.validateTextField(nameTextField) || !validateParameters())
			return;

		List<ValidationData> validations = new ArrayList<>(addedValidationsList.getItems());
		StringVariable toAdd = AddElementModel.createStringVariable(nameTextField.getText(), validations);

		AddElementModel.addStringVariableToNode(idOfTheCallingNode, toAdd, addToAllRowsCheckBox.isSelected());

		closeWindow();
	}

	boolean validateParameters(){
		for(ValidationData validationData: addedValidationsList.getItems())
			for(String validationParameter: validationData.validationParameters)
				if(validationParameter.equals("Parameter to change") || validationParameter.equals("")) {
					new AlertWindow("Wrong parameter for validation: " + validationData.validationType + " !").show();
					return false;
				}

		return true;
	}

	void closeWindow(){
		Stage stage = (Stage)mainLayout.getScene().getWindow();
		stage.fireEvent(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));
	}
}
