package windows.elementswithdatawindows;

import application.FileElementsContainer;
import elements.Element;
import elements.dataelements.ConstValuesCBox;
import elements.dataelements.FunctionCBox;
import elements.dataelements.Group;
import elements.dataelements.simpledataelements.NumericValue;
import elements.dataelements.simpledataelements.Point2d;
import elements.dataelements.simpledataelements.Point3d;
import elements.dataelements.simpledataelements.StringVariable;
import javafx.scene.control.ListView;
import treeviewcustom.treeitems.AbstractTreeItem;
import utility.TextFieldValidable;
import validation.ValidableData;
import validation.ValidationData;
import validation.ValidationResult;
import validation.Validator;
import windows.alertwindow.AlertWindow;

import java.util.List;

/**
 * Created by Krzysztof S. on 2017-08-24.
 */
class AddElementModel {

	static void addNumericValueToNode(int idOfTheCallingNode, NumericValue toAdd, boolean addToAllRowsOfThisSegment){
		if(!addToAllRowsOfThisSegment) {
			FileElementsContainer.searchElementById(idOfTheCallingNode).addComponent(toAdd);
		}else{
			// przerobic to tak by bylo ladniej:
			int parentSegmentId = ((AbstractTreeItem)FileElementsContainer.searchElementById(idOfTheCallingNode).getTreeItemHandler().getParent()).getID();
			Element parentSegment = FileElementsContainer.searchElementById(parentSegmentId);

			for(Element parentSegmentRow: parentSegment.components){
				NumericValue dataTypeCopyForEveryRow = new NumericValue(toAdd);
				parentSegmentRow.addComponent(dataTypeCopyForEveryRow);
			}
		}
	}

	static NumericValue createNumericValue(String name, List<ValidationData> validations) {
		NumericValue result = new NumericValue(name);

		result.setUpNewValidations(validations, result.numericDataField);
		return result;
	}

	static void addPoint2DToNode(int idOfTheCallingNode, Point2d toAdd, boolean addToAllRowsOfThisSegment){
		if(!addToAllRowsOfThisSegment) {
			FileElementsContainer.searchElementById(idOfTheCallingNode).addComponent(toAdd);
		}else{
			int parentSegmentId = ((AbstractTreeItem)FileElementsContainer.searchElementById(idOfTheCallingNode).getTreeItemHandler().getParent()).getID();
			Element parentSegment = FileElementsContainer.searchElementById(parentSegmentId);

			for(Element parentSegmentRow: parentSegment.components){
				Point2d dataTypeCopyForEveryRow = new Point2d(toAdd);
				parentSegmentRow.addComponent(dataTypeCopyForEveryRow);
			}
		}
	}

	static Point2d createPoint2D(String name, List<ValidationData> validationsOfField1, List<ValidationData> validationsOfField2) {
		Point2d result = new Point2d(name);

		result.setUpNewValidations(validationsOfField1, result.numericDataField1);
		result.setUpNewValidations(validationsOfField2, result.numericDataField2);
		return result;
	}

	static void addPoint3DToNode(int idOfTheCallingNode, Point3d toAdd, boolean addToAllRowsOfThisSegment){

		if(!addToAllRowsOfThisSegment) {
			FileElementsContainer.searchElementById(idOfTheCallingNode).addComponent(toAdd);
		}else{
			int parentSegmentId = ((AbstractTreeItem)FileElementsContainer.searchElementById(idOfTheCallingNode).getTreeItemHandler().getParent()).getID();
			Element parentSegment = FileElementsContainer.searchElementById(parentSegmentId);

			for(Element parentSegmentRow: parentSegment.components){
				Point3d dataTypeCopyForEveryRow = new Point3d(toAdd);
				parentSegmentRow.addComponent(dataTypeCopyForEveryRow);
			}
		}
	}

	static Point3d createPoint3D(String name, List<ValidationData> validationsOfField1, List<ValidationData> validationsOfField2, List<ValidationData> validationsOfField3) {
		Point3d result = new Point3d(name);

		result.setUpNewValidations(validationsOfField1, result.numericDataField1);
		result.setUpNewValidations(validationsOfField2, result.numericDataField2);
		result.setUpNewValidations(validationsOfField3, result.numericDataField3);
		return result;
	}

	static void addStringVariableToNode(int idOfTheCallingNode, StringVariable toAdd, boolean addToAllRowsOfThisSegment){
		if(!addToAllRowsOfThisSegment) {
			FileElementsContainer.searchElementById(idOfTheCallingNode).addComponent(toAdd);
		}else{
			int parentSegmentId = ((AbstractTreeItem)FileElementsContainer.searchElementById(idOfTheCallingNode).getTreeItemHandler().getParent()).getID();
			Element parentSegment = FileElementsContainer.searchElementById(parentSegmentId);

			for(Element parentSegmentRow: parentSegment.components){
				StringVariable dataTypeCopyForEveryRow = new StringVariable(toAdd);
				parentSegmentRow.addComponent(dataTypeCopyForEveryRow);
			}
		}
	}

	static StringVariable createStringVariable(String name, List<ValidationData> validations) {
		StringVariable result = new StringVariable(name);
		result.setUpNewValidations(validations, result.textDataField);
		return result;
	}

	static void addConstValuesCBoxToNode(int idOfTheCallingNode, String name, List<String> constValues, boolean addToAllRowsOfThisSegment){
		ConstValuesCBox toAdd = new ConstValuesCBox(name, constValues);

		if(!addToAllRowsOfThisSegment) {
			FileElementsContainer.searchElementById(idOfTheCallingNode).addComponent(toAdd);
		}else{
			int parentSegmentId = ((AbstractTreeItem)FileElementsContainer.searchElementById(idOfTheCallingNode).getTreeItemHandler().getParent()).getID();
			Element parentSegment = FileElementsContainer.searchElementById(parentSegmentId);

			for(Element parentSegmentRow: parentSegment.components){
				ConstValuesCBox dataTypeCopyForEveryRow = new ConstValuesCBox(toAdd);
				parentSegmentRow.addComponent(dataTypeCopyForEveryRow);
			}
		}
	}

	public static void addGroupToNode(int idOfTheCallingNode, Group toAdd, boolean addToAllRowsOfThisSegment) {
		if(!addToAllRowsOfThisSegment) {
			FileElementsContainer.searchElementById(idOfTheCallingNode).addComponent(toAdd);
		}else{
			int parentSegmentId = ((AbstractTreeItem)FileElementsContainer.searchElementById(idOfTheCallingNode).getTreeItemHandler().getParent()).getID();
			Element parentSegment = FileElementsContainer.searchElementById(parentSegmentId);

			for(Element parentSegmentRow: parentSegment.components){
				Group dataTypeCopyForEveryRow = new Group(toAdd);
				parentSegmentRow.addComponent(dataTypeCopyForEveryRow);
			}
		}
	}

	static void addFunctionCBoxToNode(int idOfTheCallingNode, FunctionCBox toAdd, boolean addToAllRowsOfThisSegment){
		if(!addToAllRowsOfThisSegment) {
			FileElementsContainer.searchElementById(idOfTheCallingNode).addComponent(toAdd);
		}else{
			int parentSegmentId = ((AbstractTreeItem)FileElementsContainer.searchElementById(idOfTheCallingNode).getTreeItemHandler().getParent()).getID();
			Element parentSegment = FileElementsContainer.searchElementById(parentSegmentId);

			for(Element parentSegmentRow: parentSegment.components){
				FunctionCBox dataTypeCopyForEveryRow = new FunctionCBox(toAdd);
				parentSegmentRow.addComponent(dataTypeCopyForEveryRow);
			}
		}
	}

	static boolean validateTextField(TextFieldValidable textField){
		ValidationResult singleValidationResult;
		for(ValidationData validationData: textField.getValidations()){
			singleValidationResult = Validator.validate(new ValidableData(textField.getText(), validationData));
			if(!singleValidationResult.hasValidationPassed()){
				textField.setValidationStyle(singleValidationResult.hasValidationPassed());
				return false;
			}
		}
		return true;
	}

	static boolean checkListViewContainsAnyElementAndShowAlert(ListView constValuesList, AlertWindow alertWindow){
		if(constValuesList.getItems().isEmpty()){
			alertWindow.show();
			return false;
		}
		return true;
	}

	static boolean checkListViewContainsDuplicatesAndShowAlert(ListView<String> constValuesList, String pattern, AlertWindow alertWindow){
		if(constValuesList.getItems().contains(pattern)){
			alertWindow.show();
			return false;
		}
		return true;
	}
}
