package windows.elementswithdatawindows;

import elements.ElementTag;
import elements.dataelements.Group;
import elements.dataelements.simpledataelements.ElementWithSimpleData;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import utility.TextFieldValidable;
import validation.ValidationData;
import validation.Validator;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by Krzysztof S. on 2017-08-24.
 */
public class AddGroupWindowController implements Initializable {
	@FXML
	private ComboBox<ElementTag> dataTypeComboBox;

	@FXML
	TextFieldValidable nameTextField;

	@FXML
	TextFieldValidable lengthTextField;

	@FXML
	CheckBox addToAllRowsCheckBox;

	@FXML
	private HBox dataTypeHBox;

	@FXML
	private VBox mainLayout;

	int idOfTheCallingNode;
	HashMap<ElementTag, Parent> dataTypesControllers;

	public AddGroupWindowController(int idOfTheCallingNode) throws IOException {
		this.idOfTheCallingNode = idOfTheCallingNode;
		initializeDataTypesControllers(idOfTheCallingNode);
	}

	protected void initializeDataTypesControllers(int idOfTheCallingNode) throws IOException {
		dataTypesControllers = new HashMap();

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("AddSimpleDataTypeWindowController.fxml"));
		fxmlLoader.setController(new AddNumericValueWindowController(idOfTheCallingNode){
			@Override
			void afterInitialization(){
				setupDefaultValidations();
				mainLayout.getChildren().remove(0);
			}

			@Override
			void createButtonAction(){
				if(!AddElementModel.validateTextField(AddGroupWindowController.this.nameTextField) || !AddElementModel.validateTextField(AddGroupWindowController.this.lengthTextField)
						|| !validateParameters())
					return;

				List<ValidationData> validations = new ArrayList<>(addedValidationsList.getItems());
				ElementWithSimpleData pattern = AddElementModel.createNumericValue(nameTextField.getText(), validations);

				createAndAddGroup(pattern);
				closeWindow();
			}
		});
		dataTypesControllers.put(ElementTag.NUMERIC_VALUE, fxmlLoader.load());

		fxmlLoader = new FXMLLoader(getClass().getResource("AddSimpleDataTypeWindowController.fxml"));
		fxmlLoader.setController(new AddStringVariableWindowController(idOfTheCallingNode){
			@Override
			void afterInitialization(){
				mainLayout.getChildren().remove(0);
			}

			@Override
			void createButtonAction(){
				if(!AddElementModel.validateTextField(AddGroupWindowController.this.nameTextField) || !AddElementModel.validateTextField(AddGroupWindowController.this.lengthTextField)
						|| !validateParameters())
					return;

				List<ValidationData> validations = new ArrayList<>(addedValidationsList.getItems());
				ElementWithSimpleData pattern = AddElementModel.createStringVariable(nameTextField.getText(), validations);

				createAndAddGroup(pattern);
				closeWindow();
			}
		});
		dataTypesControllers.put(ElementTag.STRING_VARIABLE, fxmlLoader.load());

		fxmlLoader = new FXMLLoader(getClass().getResource("AddPoint2DWindowController.fxml"));
		fxmlLoader.setController(new AddPoint2DWindowController(idOfTheCallingNode){
			@Override
			void afterInitialization(){
				setupDefaultValidations();
				mainLayout.getChildren().remove(0);
			}

			@Override
			void createButtonAction(){
				if(!AddElementModel.validateTextField(AddGroupWindowController.this.nameTextField) || !AddElementModel.validateTextField(AddGroupWindowController.this.lengthTextField)
						|| !validateParameters() )
					return;

				ElementWithSimpleData pattern = createElement();
				createAndAddGroup(pattern);
				closeWindow();
			}
		});
		dataTypesControllers.put(ElementTag.POINT2D, fxmlLoader.load());

		fxmlLoader = new FXMLLoader(getClass().getResource("AddPoint3DWindowController.fxml"));
		fxmlLoader.setController(new AddPoint3DWindowController(idOfTheCallingNode){
			@Override
			void afterInitialization(){
				setupDefaultValidations();
				mainLayout.getChildren().remove(0);
			}

			@Override
			void createButtonAction(){
				if(!AddElementModel.validateTextField(AddGroupWindowController.this.nameTextField) || !AddElementModel.validateTextField(AddGroupWindowController.this.lengthTextField)
						|| !validateParameters())
					return;

				ElementWithSimpleData pattern = createElement();
				createAndAddGroup(pattern);
				closeWindow();
			}
		});
		dataTypesControllers.put(ElementTag.POINT3D, fxmlLoader.load());
	}

	Group createGroup(ElementWithSimpleData pattern){
		return new Group(AddGroupWindowController.this.nameTextField.getText(), pattern, Integer.parseInt(lengthTextField.getText()));
	}

	void createAndAddGroup(ElementWithSimpleData pattern){
		Group groupToAdd = createGroup(pattern);
		AddElementModel.addGroupToNode(AddGroupWindowController.this.idOfTheCallingNode, groupToAdd, AddGroupWindowController.this.addToAllRowsCheckBox.isSelected());
	}

	public void selectElementType(ElementTag elementTag){
		if(dataTypesControllers.containsKey(elementTag))
			dataTypeComboBox.getSelectionModel().select(elementTag);
		else
			throw new IllegalArgumentException("Not supported ElementType in this group.");
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		ObservableList<ElementTag> dataTypesList = FXCollections.observableList(new ArrayList<>());
		dataTypesList.add(ElementTag.NUMERIC_VALUE);
		dataTypesList.add(ElementTag.POINT2D);
		dataTypesList.add(ElementTag.POINT3D);
		dataTypesList.add(ElementTag.STRING_VARIABLE);
		dataTypeComboBox.setItems(dataTypesList);
		dataTypeComboBox.getSelectionModel().select(0);
		dataTypeHBox.getChildren().add(dataTypesControllers.get(dataTypeComboBox.getSelectionModel().getSelectedItem()));

		dataTypeComboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<ElementTag>() {
			@Override
			public void changed(ObservableValue<? extends ElementTag> observable, ElementTag oldValue, ElementTag newValue) {
				dataTypeHBox.getChildren().clear();
				dataTypeHBox.getChildren().add(dataTypesControllers.get(newValue));
				if(mainLayout.getScene() != null)
					mainLayout.getScene().getWindow().sizeToScene();	// jesli wielkosc zawartosci okna bedzie wieksza niz samo okno, to sie ono powiekszy.
			}
		});

		nameTextField.addValidation(new ValidationData(Validator.ValidationTypes.IS_NOT_EMPTY, null));
		nameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
		nameTextField.setValidationStyle(true);
		});

		lengthTextField.addValidation(new ValidationData(Validator.ValidationTypes.IS_NOT_EMPTY, null));
		lengthTextField.addValidation(new ValidationData(Validator.ValidationTypes.IS_INTEGER, null));
		lengthTextField.addValidation(new ValidationData(Validator.ValidationTypes.IS_HIGHER_THAN, new ArrayList<String>(){{add("0");}}));
		lengthTextField.textProperty().addListener((observable, oldValue, newValue) -> {
		lengthTextField.setValidationStyle(true);
		});

		lengthTextField.setText("1");
		afterInitialization();
	}

	void afterInitialization(){}

	void closeWindow(){
		Stage stage = (Stage)mainLayout.getScene().getWindow();
		stage.fireEvent(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));
	}
}