package windows.elementswithdatawindows;

import elements.dataelements.simpledataelements.Point3d;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import utility.TextFieldValidable;
import validation.ValidationData;
import validation.Validator;
import windows.alertwindow.AlertWindow;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by Krzysztof S. on 2017-08-25.
 */
public class AddPoint3DWindowController implements Initializable{

	@FXML
	VBox mainLayout;

	@FXML
	private ComboBox<Validator.ValidationTypes> validationsCBox;

	@FXML
	ListView<ValidationData> addedValidationsList;

	@FXML
	private ListView<String> validationParamsList;

	@FXML
	TextFieldValidable nameTextField;

	@FXML
	CheckBox addToAllRowsCheckBox;

	@FXML
	CheckBox applyField1ToAllCheckBox;

	@FXML
	Button createButton;

	@FXML
	private RadioButton field1RadioButton;

	@FXML
	private RadioButton field2RadioButton;

	@FXML
	private RadioButton field3RadioButton;

	@FXML
	private ToggleGroup fieldValidationsToggleGroup;

	private int idOfTheCallingNode;
	List<ValidationData> addedValidationsForField1;
	List<ValidationData> addedValidationsForField2;
	List<ValidationData> addedValidationsForField3;

	public AddPoint3DWindowController(int idOfTheCallingNode) {
		this.idOfTheCallingNode = idOfTheCallingNode;
		addedValidationsForField1 = new ArrayList<>();
		addedValidationsForField2 = new ArrayList<>();
		addedValidationsForField3 = new ArrayList<>();
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		ObservableList<Validator.ValidationTypes> validationTypesList = FXCollections.observableList(new ArrayList<>(Arrays.asList(Validator.ValidationTypes.values())));
		validationsCBox.setItems(validationTypesList);
		validationsCBox.getSelectionModel().select(0);

		addedValidationsList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<ValidationData>() {
			@Override
			public void changed(ObservableValue<? extends ValidationData> observable, ValidationData oldValue, ValidationData newValue) {
				validationParamsList.getItems().clear();
				if(!addedValidationsList.getItems().isEmpty())
					validationParamsList.getItems().addAll(addedValidationsList.getSelectionModel().getSelectedItem().validationParameters);
			}
		});

		fieldValidationsToggleGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
			@Override
			public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
				addedValidationsList.getItems().clear();
				if(getSelectedFieldIndex() == 1)
					addedValidationsList.getItems().addAll(addedValidationsForField1);
				else if(getSelectedFieldIndex() == 2)
					addedValidationsList.getItems().addAll(addedValidationsForField2);
				else if(getSelectedFieldIndex() == 3)
					addedValidationsList.getItems().addAll(addedValidationsForField3);

				if(!addedValidationsList.getItems().isEmpty())
					addedValidationsList.getSelectionModel().select(0);
			}
		});

		validationParamsList.setCellFactory(TextFieldListCell.forListView());
		validationParamsList.setOnEditCommit(t -> {
			validationParamsList.getItems().set(t.getIndex(), t.getNewValue());
			addedValidationsList.getSelectionModel().getSelectedItem().validationParameters.set(t.getIndex(), t.getNewValue());
		});

		nameTextField.addValidation(new ValidationData(Validator.ValidationTypes.IS_NOT_EMPTY, null));
		nameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
			nameTextField.setValidationStyle(true);
		});

		afterInitialization();
	}

	void afterInitialization(){
		setupDefaultValidations();
	}

	protected void setupDefaultValidations() {
		addedValidationsForField1.add(new ValidationData(Validator.ValidationTypes.IS_NUMBER, null));
		addedValidationsForField2.add(new ValidationData(Validator.ValidationTypes.IS_NUMBER, null));
		addedValidationsForField3.add(new ValidationData(Validator.ValidationTypes.IS_NUMBER, null));
		addedValidationsList.getItems().add(addedValidationsForField1.get(0));
	}

	@FXML
	private void addValidationButtonAction(){
		Validator.ValidationTypes selectedValidationInCBox = validationsCBox.getSelectionModel().getSelectedItem();
		if(selectedValidationInCBox != null){
			ArrayList<String> params = new ArrayList<>();
			for (int paramCounter=0; paramCounter < selectedValidationInCBox.getNumberOfParameters(); paramCounter++) {
				params.add("Parameter to change");
			}

			ValidationData validationToAdd = new ValidationData(selectedValidationInCBox, params);
			if(getSelectedFieldIndex() == 1)
				addedValidationsForField1.add(validationToAdd);
			else if(getSelectedFieldIndex() == 2)
				addedValidationsForField2.add(validationToAdd);
			else if(getSelectedFieldIndex() == 3)
				addedValidationsForField3.add(validationToAdd);

			addedValidationsList.getItems().add(validationToAdd);
		}
	}

	/**
	 * @return 0 if nothing is selected.
	 */
	private int getSelectedFieldIndex(){
		RadioButton selectedRadioButton = (RadioButton) fieldValidationsToggleGroup.getSelectedToggle();
		if(selectedRadioButton.getText().equals(field1RadioButton.getText()))
			return 1;
		else if(selectedRadioButton.getText().equals(field2RadioButton.getText()))
			return 2;
		else if(selectedRadioButton.getText().equals(field3RadioButton.getText()))
			return 3;
		else
			return 0;
	}

	@FXML
	private void removaButtonAction(){
		if(getSelectedFieldIndex() == 1)
			addedValidationsForField1.remove(addedValidationsList.getSelectionModel().getSelectedItem());
		else if(getSelectedFieldIndex() == 2)
			addedValidationsForField2.remove(addedValidationsList.getSelectionModel().getSelectedItem());
		else if(getSelectedFieldIndex() == 3)
			addedValidationsForField3.remove(addedValidationsList.getSelectionModel().getSelectedItem());

		addedValidationsList.getItems().remove(addedValidationsList.getSelectionModel().getSelectedItem());
	}

	@FXML
	void createButtonAction(){
		if(!AddElementModel.validateTextField(nameTextField) || !validateParameters())
			return;

		Point3d elementToAdd = createElement();
		AddElementModel.addPoint3DToNode(idOfTheCallingNode, elementToAdd, addToAllRowsCheckBox.isSelected());

		closeWindow();
	}

	boolean validateParameters(){
		for(ValidationData validationData: addedValidationsForField1)
			for(String validationParameter: validationData.validationParameters)
				if(validationParameter.equals("Parameter to change") || validationParameter.equals("")) {
					new AlertWindow("Wrong parameter for validation: " + validationData.validationType + " of field1!").show();
					return false;
				}

		for(ValidationData validationData: addedValidationsForField2)
			for(String validationParameter: validationData.validationParameters)
				if(validationParameter.equals("Parameter to change") || validationParameter.equals("")) {
					new AlertWindow("Wrong parameter for validation: " + validationData.validationType + " of field2!").show();
					return false;
				}

		for(ValidationData validationData: addedValidationsForField3)
			for(String validationParameter: validationData.validationParameters)
				if(validationParameter.equals("Parameter to change") || validationParameter.equals("")) {
					new AlertWindow("Wrong parameter for validation: " + validationData.validationType + " of field3!").show();
					return false;
				}

		return true;
	}

	Point3d createElement() {
		Point3d elementToAdd;
		if(!applyField1ToAllCheckBox.isSelected())
			elementToAdd = AddElementModel.createPoint3D(nameTextField.getText(), addedValidationsForField1, addedValidationsForField2, addedValidationsForField3);
		else{
			ArrayList<ValidationData> copy1OfField1Validations = new ArrayList<>();
			for(ValidationData validationToCopy: addedValidationsForField1)
				copy1OfField1Validations.add(new ValidationData(validationToCopy.validationType, validationToCopy.validationParameters));

			ArrayList<ValidationData> copy2OfField1Validations = new ArrayList<>();
			for(ValidationData validationToCopy: addedValidationsForField1)
				copy2OfField1Validations.add(new ValidationData(validationToCopy.validationType, validationToCopy.validationParameters));

			elementToAdd = AddElementModel.createPoint3D(nameTextField.getText(), addedValidationsForField1, copy1OfField1Validations, copy2OfField1Validations);
		}
		return elementToAdd;
	}

	void closeWindow(){
		Stage stage = (Stage)mainLayout.getScene().getWindow();
		stage.fireEvent(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));
	}

}
