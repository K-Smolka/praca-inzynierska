package windows.elementswithdatawindows;

import application.FileElementsContainer;
import elements.ElementTag;
import elements.dataelements.*;
import elements.dataelements.simpledataelements.*;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.layout.VBox;
import utility.TextFieldValidable;
import validation.ValidationData;
import windows.alertwindow.AlertWindow;

import java.io.IOException;
import java.net.URL;
import java.util.*;

/**
 * Created by Krzysztof S. on 2017-09-19.
 */
public class EditDataElementWindowController implements Initializable {

	@FXML
	VBox mainLayout;

	int idOfTheCallingNode;
	Parent usedElementController;

	public EditDataElementWindowController(int idOfTheCallingNode) throws IOException {
			this.idOfTheCallingNode = idOfTheCallingNode;
			ElementWithData elementToEdit = (ElementWithData)FileElementsContainer.searchElementById(idOfTheCallingNode);

			switch(elementToEdit.elementTag){
				case NUMERIC_VALUE:
					usedElementController = createElementController((NumericValue) elementToEdit);
					break;

				case POINT2D:
					usedElementController = createElementController((Point2d) elementToEdit);
					break;

				case POINT3D:
					usedElementController = createElementController((Point3d) elementToEdit);
					break;

				case STRING_VARIABLE:
					usedElementController = createElementController((StringVariable) elementToEdit);
					break;

				case CONST_VALUES_CBOX:
					usedElementController = createElementController((ConstValuesCBox) elementToEdit);
					break;

				case GROUP:
					usedElementController = createElementController((Group) elementToEdit);
					break;

				case FUNCTION_CBOX:
					usedElementController = createElementController((FunctionCBox) elementToEdit);
					break;

				default:
					throw new IllegalArgumentException("Nieobslugiwany format!");
			}
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		mainLayout.getChildren().add(usedElementController);
	}

	private Parent createElementController(NumericValue elementToEdit) throws IOException{
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("AddSimpleDataTypeWindowController.fxml"));
		fxmlLoader.setController(new AddNumericValueWindowController(idOfTheCallingNode){
			@Override
			void afterInitialization(){
				addToAllRowsCheckBox.setDisable(true);
				createButton.setText("Apply");

				nameTextField.setText(elementToEdit.getName());
				addedValidationsList.getItems().addAll(elementToEdit.numericDataField.getValidations());
			}
			@Override
			void createButtonAction(){
				if(!AddElementModel.validateTextField(nameTextField) || !validateParameters())
					return;

				elementToEdit.setUpVariableName(nameTextField.getText());
				elementToEdit.numericDataField.setUpNewValidations(new ArrayList<>(addedValidationsList.getItems()));

				super.closeWindow();
			}
		});
		return fxmlLoader.load();
	}
	private Parent createElementController(Point2d elementToEdit) throws IOException{
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("AddPoint2DWindowController.fxml"));
		fxmlLoader.setController(new AddPoint2DWindowController(idOfTheCallingNode){
			@Override
			void afterInitialization(){
				addToAllRowsCheckBox.setDisable(true);
				createButton.setText("Apply");

				nameTextField.setText(elementToEdit.getName());
				addedValidationsForField1.addAll(elementToEdit.numericDataField1.getValidations());
				addedValidationsForField2.addAll(elementToEdit.numericDataField2.getValidations());
				addedValidationsList.getItems().addAll(addedValidationsForField1);
			}
			@Override
			void createButtonAction(){
				if(!AddElementModel.validateTextField(nameTextField) || !validateParameters())
					return;

				elementToEdit.setUpVariableName(nameTextField.getText());
				elementToEdit.numericDataField1.setUpNewValidations(addedValidationsForField1);
				elementToEdit.numericDataField2.setUpNewValidations(addedValidationsForField2);

				super.closeWindow();
			}
		});
		return fxmlLoader.load();
	}
	private Parent createElementController(Point3d elementToEdit) throws IOException{
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("AddPoint3DWindowController.fxml"));
		fxmlLoader.setController(new AddPoint3DWindowController(idOfTheCallingNode){
			@Override
			void afterInitialization(){
				addToAllRowsCheckBox.setDisable(true);
				createButton.setText("Apply");

				nameTextField.setText(elementToEdit.getName());
				addedValidationsForField1.addAll(elementToEdit.numericDataField1.getValidations());
				addedValidationsForField2.addAll(elementToEdit.numericDataField2.getValidations());
				addedValidationsForField3.addAll(elementToEdit.numericDataField3.getValidations());
				addedValidationsList.getItems().addAll(addedValidationsForField1);
			}
			@Override
			void createButtonAction(){
				if(!AddElementModel.validateTextField(nameTextField) || !validateParameters())
					return;

				elementToEdit.setUpVariableName(nameTextField.getText());
				elementToEdit.numericDataField1.setUpNewValidations(addedValidationsForField1);
				elementToEdit.numericDataField2.setUpNewValidations(addedValidationsForField2);
				elementToEdit.numericDataField3.setUpNewValidations(addedValidationsForField3);

				super.closeWindow();
			}
		});
		return fxmlLoader.load();
	}
	private Parent createElementController(StringVariable elementToEdit) throws IOException{
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("AddSimpleDataTypeWindowController.fxml"));
		fxmlLoader.setController(new AddStringVariableWindowController(idOfTheCallingNode){
			@Override
			void afterInitialization(){
				addToAllRowsCheckBox.setDisable(true);
				createButton.setText("Apply");

				nameTextField.setText(elementToEdit.getName());
				addedValidationsList.getItems().addAll(elementToEdit.textDataField.getValidations());
			}
			@Override
			void createButtonAction(){
				if(!AddElementModel.validateTextField(nameTextField) || !validateParameters())
					return;

				elementToEdit.setUpVariableName(nameTextField.getText());
				elementToEdit.textDataField.setUpNewValidations(new ArrayList<>(addedValidationsList.getItems()));

				super.closeWindow();
			}
		});
		return fxmlLoader.load();
	}
	private Parent createElementController(ConstValuesCBox elementToEdit) throws IOException{
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("AddContValuesCBoxWindowController.fxml"));
		fxmlLoader.setController(new AddContValuesCBoxWindowController(idOfTheCallingNode){
			@Override
			void afterInitialization(){
				addToAllRowsCheckBox.setDisable(true);
				createButton.setText("Apply");

				nameTextField.setText(elementToEdit.getName());
				addedConstValuesList.getItems().addAll(elementToEdit.consts);
			}
			@Override
			void createButtonAction(){
				if(!AddElementModel.validateTextField(nameTextField) || !AddElementModel.checkListViewContainsAnyElementAndShowAlert(addedConstValuesList, new AlertWindow("Const values list can't be empty!")))
					return;

				String oldSelectedConst = elementToEdit.getSelectedConst();
				elementToEdit.setUpVariableName(nameTextField.getText());
				elementToEdit.consts.clear();

				for (String constValueToAdd: addedConstValuesList.getItems())
					elementToEdit.addNewConst(constValueToAdd);

				if(addedConstValuesList.getItems().contains(oldSelectedConst))
					elementToEdit.selectValue(oldSelectedConst);

				super.closeWindow();
			}
		});
		return fxmlLoader.load();
	}
	private Parent createElementController(Group elementToEdit) throws IOException{

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("AddGroupWindowController.fxml"));
		fxmlLoader.setController(new AddGroupWindowController(idOfTheCallingNode){
			@Override
			void afterInitialization(){
				addToAllRowsCheckBox.setDisable(true);

				nameTextField.setText(elementToEdit.getName());
				lengthTextField.setText(elementToEdit.components.size() + "");
				selectElementType(elementToEdit.patternToGroup.elementTag);
			}
			@Override
			protected void initializeDataTypesControllers(int idOfTheCallingNode) throws IOException {
				dataTypesControllers = new HashMap();

				FXMLLoader elementFXMLLoader = new FXMLLoader(getClass().getResource("AddSimpleDataTypeWindowController.fxml"));
				elementFXMLLoader.setController(new AddNumericValueWindowController(idOfTheCallingNode){
					@Override
					void afterInitialization(){
						mainLayout.getChildren().remove(0);
						createButton.setText("Apply");

						if(elementToEdit.patternToGroup.elementTag.equals(ElementTag.NUMERIC_VALUE)){
							nameTextField.setText(elementToEdit.getName());
							addedValidationsList.getItems().addAll(((NumericValue)elementToEdit.patternToGroup).numericDataField.getValidations());
						}
					}

					@Override
					void createButtonAction(){
						TextFieldValidable groupLengthFieldHandler = ((AddGroupWindowController)fxmlLoader.getController()).lengthTextField;
						TextFieldValidable groupNameTextFieldHandler = ((AddGroupWindowController)fxmlLoader.getController()).nameTextField;

						if(!AddElementModel.validateTextField(groupNameTextFieldHandler) || !AddElementModel.validateTextField(groupLengthFieldHandler)
								|| !validateParameters())
							return;

						List<ValidationData> validations = new ArrayList<>(addedValidationsList.getItems());
						ElementWithSimpleData pattern = AddElementModel.createNumericValue(nameTextField.getText(), validations);

						elementToEdit.setNewPattern(pattern);
						elementToEdit.setNewListSize(Integer.parseInt(lengthTextField.getText()));

						closeWindow();
					}
				});
				dataTypesControllers.put(ElementTag.NUMERIC_VALUE, elementFXMLLoader.load());

				elementFXMLLoader = new FXMLLoader(getClass().getResource("AddSimpleDataTypeWindowController.fxml"));
				elementFXMLLoader.setController(new AddStringVariableWindowController(idOfTheCallingNode){
					@Override
					void afterInitialization(){
						mainLayout.getChildren().remove(0);
						createButton.setText("Apply");

						if(elementToEdit.patternToGroup.elementTag.equals(ElementTag.STRING_VARIABLE)){
							nameTextField.setText(elementToEdit.getName());
							addedValidationsList.getItems().addAll(((StringVariable)elementToEdit.patternToGroup).textDataField.getValidations());
						}
					}

					@Override
					void createButtonAction(){
						TextFieldValidable groupLengthFieldHandler = ((AddGroupWindowController)fxmlLoader.getController()).lengthTextField;
						TextFieldValidable groupNameTextFieldHandler = ((AddGroupWindowController)fxmlLoader.getController()).nameTextField;

						if(!AddElementModel.validateTextField(groupNameTextFieldHandler) || !AddElementModel.validateTextField(groupLengthFieldHandler)
								|| !validateParameters())
							return;

						List<ValidationData> validations = new ArrayList<>(addedValidationsList.getItems());
						ElementWithSimpleData pattern = AddElementModel.createStringVariable(nameTextField.getText(), validations);

						elementToEdit.setNewPattern(pattern);
						elementToEdit.setNewListSize(Integer.parseInt(lengthTextField.getText()));

						closeWindow();
					}
				});
				dataTypesControllers.put(ElementTag.STRING_VARIABLE, elementFXMLLoader.load());

				elementFXMLLoader = new FXMLLoader(getClass().getResource("AddPoint2DWindowController.fxml"));
				elementFXMLLoader.setController(new AddPoint2DWindowController(idOfTheCallingNode){
					@Override
					void afterInitialization(){
						mainLayout.getChildren().remove(0);
						createButton.setText("Apply");

						if(elementToEdit.patternToGroup.elementTag.equals(ElementTag.POINT2D)){
							nameTextField.setText(elementToEdit.getName());
							addedValidationsForField1.addAll(((Point2d)elementToEdit.patternToGroup).numericDataField1.getValidations());
							addedValidationsForField2.addAll(((Point2d)elementToEdit.patternToGroup).numericDataField2.getValidations());
							addedValidationsList.getItems().addAll(((Point2d)elementToEdit.patternToGroup).numericDataField1.getValidations());
						}
					}

					@Override
					void createButtonAction(){
						TextFieldValidable groupLengthFieldHandler = ((AddGroupWindowController)fxmlLoader.getController()).lengthTextField;
						TextFieldValidable groupNameTextFieldHandler = ((AddGroupWindowController)fxmlLoader.getController()).nameTextField;

						if(!AddElementModel.validateTextField(groupNameTextFieldHandler) || !AddElementModel.validateTextField(groupLengthFieldHandler)
								|| !validateParameters() )
							return;

						ElementWithSimpleData pattern = createElement();

						elementToEdit.setNewPattern(pattern);
						elementToEdit.setNewListSize(Integer.parseInt(lengthTextField.getText()));

						closeWindow();
					}
				});
				dataTypesControllers.put(ElementTag.POINT2D, elementFXMLLoader.load());

				elementFXMLLoader = new FXMLLoader(getClass().getResource("AddPoint3DWindowController.fxml"));
				elementFXMLLoader.setController(new AddPoint3DWindowController(idOfTheCallingNode){
					@Override
					void afterInitialization(){
						mainLayout.getChildren().remove(0);
						createButton.setText("Apply");

						if(elementToEdit.patternToGroup.elementTag.equals(ElementTag.POINT3D)){
							nameTextField.setText(elementToEdit.getName());
							addedValidationsForField1.addAll(((Point3d)elementToEdit.patternToGroup).numericDataField1.getValidations());
							addedValidationsForField2.addAll(((Point3d)elementToEdit.patternToGroup).numericDataField2.getValidations());
							addedValidationsForField3.addAll(((Point3d)elementToEdit.patternToGroup).numericDataField3.getValidations());
							addedValidationsList.getItems().addAll(((Point3d)elementToEdit.patternToGroup).numericDataField1.getValidations());
						}
					}

					@Override
					void createButtonAction(){
						TextFieldValidable groupLengthFieldHandler = ((AddGroupWindowController)fxmlLoader.getController()).lengthTextField;
						TextFieldValidable groupNameTextFieldHandler = ((AddGroupWindowController)fxmlLoader.getController()).nameTextField;

						if(!AddElementModel.validateTextField(groupNameTextFieldHandler) || !AddElementModel.validateTextField(groupLengthFieldHandler)
								|| !validateParameters())
							return;

						ElementWithSimpleData pattern = createElement();

						elementToEdit.setNewPattern(pattern);
						elementToEdit.setNewListSize(Integer.parseInt(lengthTextField.getText()));

						closeWindow();
					}
				});
				dataTypesControllers.put(ElementTag.POINT3D, elementFXMLLoader.load());
			}
		});
		return fxmlLoader.load();
	}
	private Parent createElementController(FunctionCBox elementToEdit) throws IOException{
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("AddFunctionCBoxWindowController.fxml"));
		fxmlLoader.setController(new AddFunctionCBoxWindowController(idOfTheCallingNode){
			@Override
			void afterInitialization(){
				addToAllRowsCheckBox.setDisable(true);
				createButton.setText("Apply");

				for(Function function: elementToEdit.functions){
					List<ElementWithData> functionComponents = (List<ElementWithData>)(List<?>)function.components;
					functionsMap.put(function.getName(), functionComponents);
					functionsListView.getItems().add(function.getName());
				}

			}
			@Override
			void createButtonAction(){
				List<Function> newFunctions = new ArrayList<>();

				for (Map.Entry<String, List<ElementWithData>> functionMapEntry :functionsMap.entrySet()) {
					newFunctions.add(new Function(functionMapEntry.getKey(), functionMapEntry.getValue()));
				}

				if(!validateFunctionsListIsEmpty())
					return;

				elementToEdit.removeAllFunctions();
				for(Function newFunction: newFunctions)
					elementToEdit.addNewFunction(newFunction);
				super.closeWindow();
			}
		});
		return fxmlLoader.load();
	}
}
