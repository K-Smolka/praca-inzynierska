package treeviewcustom.treeitems;

import application.FileElementsContainer;
import elements.Element;
import javafx.scene.Parent;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.stage.Stage;
import windows.alertwindow.AlertWindow;
import windows.editnamewindow.EditElementNameWindow;

public abstract class AbstractTreeItem extends TreeItem {

	protected ContextMenu contextMenu;
	protected Parent editNameWindowHandler;

	/**
	 * id odpowiadajacego mu UseableByTreeView
	 */
	protected final int ID;

	public AbstractTreeItem(String name, int id) {
		super(name);
		this.ID = id;
		contextMenu = createContextMenu();
	}

	protected abstract ContextMenu createContextMenu();

	public ContextMenu getContextMenu() {
		return contextMenu;
	}

	public int getID() {
		return ID;
	}

	protected MenuItem[] createBasicContextMenuItems(){

		MenuItem editNode = new MenuItem("Edit name");
		MenuItem deleteNode = new MenuItem("Delete");

		MenuItem[] contextMenuItems = {editNode, deleteNode};

		editNode.setOnAction(event -> {
			if(editNameWindowHandler==null)
				createAndShowEditNameWindow();
			else
				((Stage)editNameWindowHandler.getScene().getWindow()).toFront();
		});

		deleteNode.setOnAction(event -> {
			deleteThisTreeItemAndAssociatedWorkspaceElement();
		});

		return contextMenuItems;
	}

	protected void createAndShowEditNameWindow() {
		EditElementNameWindow editWindow = new EditElementNameWindow(this);
		editNameWindowHandler = editWindow.getRoot();
		editWindow.show();
	}

	//Wywolywana przez buttonOK z EditElementNameWindowController:
	public void editName(){
		if(editNameWindowHandler == null)
			return;

		// get(0) bo pod indeksem 0, jest TestField w FXMLu editWindow:
		String newName = ((TextField) editNameWindowHandler.getChildrenUnmodifiable().get(0)).getText();
		editName(newName);

		closeEditWindow();
	}

	//Wywolywana przy zamknięciu EditElementNameWindow:
	public void closeEditWindow(){
		editNameWindowHandler = null;
	}

	protected void editName(String newName){
		Element itemToRename = FileElementsContainer.searchElementById(ID);
		if(itemToRename != null)
			itemToRename.setName(newName);
		else
			throw new NullPointerException("Nie znaleziono UseableByTreeViewInterface o zadanym ID!");
		this.setValue(newName);
	}

	protected void deleteThisTreeItem() {
		getParent().getChildren().remove(this);
	}

	protected boolean deleteThisTreeItemAndAssociatedWorkspaceElement(){
		if(FileElementsContainer.deleteObject(ID)) {
			deleteThisTreeItem();
			return true;
		}
		return false;
	}

	public boolean deleteTreeItemByID(int id){
		if(this.getID() == id){
			deleteThisTreeItemAndAssociatedWorkspaceElement();
			return true;
		}

		for(int i=0; i<this.getChildren().size(); i++){
			if(((AbstractTreeItem)this.getChildren().get(i)).deleteTreeItemByID(id) == true){
				return true;
			}
		}
		return false;
	}
}
