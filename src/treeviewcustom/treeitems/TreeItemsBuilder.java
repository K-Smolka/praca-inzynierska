package treeviewcustom.treeitems;

import application.FileElementsContainer;
import elements.Element;
import elements.ElementTag;
import elements.FileSegment;
import elements.Row;
import elements.dataelements.FunctionCBox;
import elements.dataelements.Function;
import elements.dataelements.Group;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import windows.elementswithdatawindows.AddDataElementWindow;
import windows.elementswithdatawindows.EditDataElementWindow;
import windows.addsegmentwindow.AddSegmentWindow;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Krzysztof S. on 2017-09-18.
 */
public class TreeItemsBuilder {
	public static AbstractTreeItem createTreeItem(String name, int id, ElementTag elementTag){
		AbstractTreeItem resultTreeItem;
		ArrayList<MenuItem> contextMenuItemsList = new ArrayList<>();

		switch (elementTag){
			case FILE:
				resultTreeItem = new AbstractTreeItem(name, id) {
					@Override
					protected ContextMenu createContextMenu() {
						MenuItem addElement = new MenuItem("Add segment");
						addElement.setOnAction(event -> new AddSegmentWindow(ID).show());

						contextMenuItemsList.add(addElement);
						contextMenuItemsList.addAll(Arrays.asList(createBasicContextMenuItems()));

						return buildContextMenu(contextMenuItemsList);
					}
				};
				break;

			case SEGMENT:
				resultTreeItem = new AbstractTreeItem(name, id) {
					@Override
					protected ContextMenu createContextMenu() {
						MenuItem addElement = new MenuItem("Add row");
						addElement.setOnAction(event -> {
							int newRowId = ((FileSegment) FileElementsContainer.searchElementById(ID)).getRowNumberForNewRow();
							Element parentSegmentElement = FileElementsContainer.searchElementById(ID);
							parentSegmentElement.addComponent(new Row(newRowId));
							parentSegmentElement.getTreeItemHandler().setExpanded(true);
						});

						contextMenuItemsList.add(addElement);
						contextMenuItemsList.addAll(Arrays.asList(createBasicContextMenuItems()));

						return buildContextMenu(contextMenuItemsList);
					}
				};
				break;

			case ROW:
				resultTreeItem = new AbstractTreeItem(name, id) {
					@Override
					protected ContextMenu createContextMenu() {
						MenuItem addElement = new MenuItem("Add element");
						addElement.setOnAction(event -> new AddDataElementWindow(ID).show());

						contextMenuItemsList.add(addElement);
						contextMenuItemsList.addAll(Arrays.asList(createBasicContextMenuItems()));
						contextMenuItemsList.remove(1);

						return buildContextMenu(contextMenuItemsList);
					}
				};
				break;

			case NUMERIC_VALUE:
				resultTreeItem = new AbstractTreeItem(name, id) {
					@Override
					protected ContextMenu createContextMenu() {
						contextMenuItemsList.add(createEditMenuItem(ID));
						contextMenuItemsList.addAll(Arrays.asList(createBasicContextMenuItems()));

						return buildContextMenu(contextMenuItemsList);
					}
				};
				break;

			case POINT2D:
				resultTreeItem = new AbstractTreeItem(name, id) {
					@Override
					protected ContextMenu createContextMenu() {
						contextMenuItemsList.add(createEditMenuItem(ID));
						contextMenuItemsList.addAll(Arrays.asList(createBasicContextMenuItems()));

						return buildContextMenu(contextMenuItemsList);
					}
				};
				break;

			case POINT3D:
				resultTreeItem = new AbstractTreeItem(name, id) {
					@Override
					protected ContextMenu createContextMenu() {
						contextMenuItemsList.add(createEditMenuItem(ID));
						contextMenuItemsList.addAll(Arrays.asList(createBasicContextMenuItems()));

						return buildContextMenu(contextMenuItemsList);
					}
				};
				break;

			case STRING_VARIABLE:
				resultTreeItem = new AbstractTreeItem(name, id) {
					@Override
					protected ContextMenu createContextMenu() {
						contextMenuItemsList.add(createEditMenuItem(ID));
						contextMenuItemsList.addAll(Arrays.asList(createBasicContextMenuItems()));

						return buildContextMenu(contextMenuItemsList);
					}
				};
				break;

			case GROUP:
				resultTreeItem = new AbstractTreeItem(name, id) {
					@Override
					protected ContextMenu createContextMenu() {
						MenuItem addElement = new MenuItem("Add element");
						addElement.setOnAction(event -> ((Group) FileElementsContainer.searchElementById(ID)).addSignleRow());

						contextMenuItemsList.add(addElement);
						contextMenuItemsList.add(createEditMenuItem(ID));
						contextMenuItemsList.addAll(Arrays.asList(createBasicContextMenuItems()));

						return buildContextMenu(contextMenuItemsList);
					}
				};
				break;

			case CONST_VALUES_CBOX:
				resultTreeItem = new AbstractTreeItem(name, id) {
					@Override
					protected ContextMenu createContextMenu() {
						contextMenuItemsList.add(createEditMenuItem(ID));
						contextMenuItemsList.addAll(Arrays.asList(createBasicContextMenuItems()));

						return buildContextMenu(contextMenuItemsList);
					}
				};
				break;

			case FUNCTION_CBOX:
				resultTreeItem = new AbstractTreeItem(name, id) {
					@Override
					protected void editName(String newName){
						FunctionCBox functionCBox = (FunctionCBox)FileElementsContainer.searchElementById(ID);
						Function itemToRename = functionCBox.selectedFunction;
						if(itemToRename != null){
							itemToRename.setName(newName);
							functionCBox.refreshComboBox();
						}else
							throw new NullPointerException("Nie znaleziono UseableByTreeViewInterface o zadanym ID!");
						this.setValue(newName);
					}

					@Override
					protected ContextMenu createContextMenu() {
						contextMenuItemsList.add(createEditMenuItem(ID));
						contextMenuItemsList.addAll(Arrays.asList(createBasicContextMenuItems()));

						return buildContextMenu(contextMenuItemsList);
					}
				};
				break;

			default:
				resultTreeItem = new AbstractTreeItem(name, id) {
					@Override
					protected ContextMenu createContextMenu() {
						contextMenuItemsList.addAll(Arrays.asList(createBasicContextMenuItems()));
						return buildContextMenu(contextMenuItemsList);
					}
				};
		}
		return resultTreeItem;
	}

	private static MenuItem createEditMenuItem(int ID) {
		MenuItem editElement = new MenuItem("Edit element");
		editElement.setOnAction(event -> new EditDataElementWindow(ID).show());
		return editElement;
	}

	private static ContextMenu buildContextMenu(List<MenuItem> listOfMenuItems){
		return new ContextMenu(Arrays.copyOf(listOfMenuItems.toArray(), listOfMenuItems.size(), MenuItem[].class));
	}
}
