package treeviewcustom;

import javafx.scene.control.TreeCell;
import treeviewcustom.treeitems.AbstractTreeItem;

public class TreeCellWithContextMenu extends TreeCell<String> {
	
    @Override
    public void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);
        
        if (empty) {
            setText(null);
            setGraphic(null);
        } else {
            setText(getItem() == null ? "" : getItem().toString());
            setGraphic(getTreeItem().getGraphic());
            setContextMenu(((AbstractTreeItem) getTreeItem()).getContextMenu());
        }
    }
}
