package treeviewcustom;

import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeView;
import javafx.util.Callback;

public class TreeViewWithContextMenu<T> extends TreeView{
	public TreeViewWithContextMenu() {
		super();
		this.setCellFactory((Callback<TreeView<String>, TreeCell<String>>) p -> new TreeCellWithContextMenu());
	}
}
