package application;

import elements.Element;
import javafx.scene.layout.Pane;

import java.util.ArrayList;

/**
 * Created by Krzysztof S. on 2017-09-01.
 */
public class FileElementsContainer {
	public static ArrayList<Element> files = new ArrayList<>();	//zrobic to read-only, i sprawdzac przy dodawaniu czy jest ok.

	public static Element searchElementById(int id){
		Element result;

		for (Element file : files) {
			if (file.ID == id)
				result = file;
			else
				result = file.searchAmongChildrensById(id);

			if (result != null) {
				return result;
			}
		}
		return null;
	}

	//przerobic na ladniejsza, powinno sie dac:
	public static boolean deleteObject(int id){
		for(int i=0; i<files.size(); i++){
			if(files.get(i).ID == id){
				// usuwanie layoutu usuwanego CompleteFile z workspace:
				((Pane) files.get(i).getFullLayout().getParent()).getChildren().remove(files.get(i).getFullLayout());

				files.remove(i);
				return true;
			}

			if(files.get(i).deleteChildrenComponentById(id))
				return true;
		}
		return false;
	}

}
