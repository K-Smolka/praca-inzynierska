package application;

import elements.CompleteFile;
import elements.Element;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TreeItem;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import treeviewcustom.TreeViewWithContextMenu;
import treeviewcustom.treeitems.AbstractTreeItem;
import validation.ValidationResult;
import validation.Validator;
import windows.aboutwindow.AboutWindow;
import windows.alertwindow.AlertWindow;
import xmlconverter.GuiXMLConverter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class MainWindowController implements Initializable{

	@FXML
	private TreeViewWithContextMenu tree;

	@FXML
	private VBox workspacePane;

	@FXML
	private Pane rootLayout;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
//		workspacePane.setPrefSize(Double.MAX_VALUE, Double.MAX_VALUE);
	}

	private void addNewFile(Element newFile){
		FileElementsContainer.files.add(newFile);
		addNewValueToWorkspace(newFile);
		tree.getRoot().getChildren().add(createNode(newFile));
	}

	@FXML
	public void exportGUIToXML(){

		try{
			if (FileElementsContainer.files.isEmpty()){
				new AlertWindow("There is no files to export!").show();
				return;
			}

			if (areFileNamesDuplicated()){
				new AlertWindow("Two files can't have the same name").show();
				return;
			}

			DirectoryChooser directoryChooser = new DirectoryChooser();
			directoryChooser.setInitialDirectory(new File("."));
			File file = directoryChooser.showDialog(rootLayout.getScene().getWindow());

			if(file != null){
				GuiXMLConverter.exportGUIToXMLFiles(file);
			}
		}catch(Exception e){
			new AlertWindow("Blad przy eksporcie GUI do XML.").show();
		}

	}

	private boolean areFileNamesDuplicated() {
		int counter = 0;
		for (Element file: FileElementsContainer.files)
			for (Element fileToCompare: FileElementsContainer.files)
				if(file.getName().equals(fileToCompare.getName()))
					counter++;

		if(counter!=FileElementsContainer.files.size())
			return true;

		return false;
	}

	@FXML
	public void importGUIFromXML(){

		try{
			FileChooser fileChooser = new FileChooser();
			FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml");
			fileChooser.getExtensionFilters().add(extFilter);
			fileChooser.setInitialDirectory(new File("."));
			File file = fileChooser.showOpenDialog(rootLayout.getScene().getWindow());

			if(file != null){
				Element completeFileToAdd = GuiXMLConverter.importGuiFromXMLFile(file);
				addNewFile(completeFileToAdd);
			}
		}catch(Exception e){
			new AlertWindow("Blad przy imporcie GUI z XML.").show();
		}
	}

	public boolean deleteByID(int id){
		for(int i=0; i<tree.getRoot().getChildren().size(); i++){
			if(((AbstractTreeItem)tree.getRoot().getChildren().get(i)).deleteTreeItemByID(id))
				return true;
		}
		return false;
	}

	private void addNewValueToWorkspace(Element completeFile){
		workspacePane.getChildren().add(completeFile.getFullLayout());
	}

	private TreeItem createNode(Element completeFile){
		return completeFile.getTreeItemHandler();
	}

	@FXML
	private void exportToFile(){
		FileWriter outFile;

		if (FileElementsContainer.files.isEmpty()){
			new AlertWindow("There is no files to export!").show();
			return;
		}

		if (areFileNamesDuplicated()){
			new AlertWindow("Two files can't have the same name").show();
			return;
		}

		DirectoryChooser directoryChooser = new DirectoryChooser();
		directoryChooser.setInitialDirectory(new File("."));
		File file = directoryChooser.showDialog(rootLayout.getScene().getWindow());

		if(file != null){
			try {
				StringBuilder errorMessage = new StringBuilder();
				for (Element fileToExport: FileElementsContainer.files) {

					String validationErrorMessage = validateFileAndReturnResultInfo(fileToExport);
					if (validationErrorMessage != null){
						errorMessage.append(validationErrorMessage);
						continue;
					}

					outFile = new FileWriter(file.getPath() + "\\" + fileToExport.getName() + ".txt");
					outFile.write(fileToExport.getDataString(0));
					outFile.write(System.lineSeparator());
					outFile.close();
				}
				if(errorMessage.length()!=0)
					new AlertWindow(errorMessage.toString()).show();

			} catch (IOException e) {
				new AlertWindow("Error during file saving!").show();
				e.printStackTrace();
			}
		}
	}

	/**
	 * @param fileToExport
	 * @return Returns null if validation passed, otherwise returns validation error message.
	 */
	private String validateFileAndReturnResultInfo(Element fileToExport) {
		List<ValidationResult> fileValidationResults = Validator.validateFile(fileToExport);
		if(!fileValidationResults.get(0).hasValidationPassed()) {
			StringBuilder validationAlertMessage = new StringBuilder();
			validationAlertMessage.append("File: \""+ fileToExport.getName()+"\" validation failed!");
			for(ValidationResult fileValidationResult: fileValidationResults)
				validationAlertMessage.append(System.lineSeparator() + fileValidationResult.getValidationInfoMessage());

			validationAlertMessage.append(System.lineSeparator() + System.lineSeparator());
			return validationAlertMessage.toString();
		}
		return null;
	}

	@FXML
	private void collapseAll(){
		collapseChildrens(tree.getRoot());
	}
	
	private void collapseChildrens(TreeItem treeItem){
		for(int i=0; i<treeItem.getChildren().size(); i++){
			((TreeItem)treeItem.getChildren().get(i)).setExpanded(false);
			collapseChildrens((TreeItem)treeItem.getChildren().get(i));
		}
	}

	@FXML
	private void addNewEmptyFile(){
		addNewFile(new CompleteFile("Empty file"));
	}

	@FXML
	private void openAboutWindow(){
		new AboutWindow().show();
	}

	@FXML
	private void closeApplication(){
		System.exit(0);
	}
}
