package xmlconverter;

/**
 * Created by Krzysztof S. on 2017-08-01.
 */
public interface ConvertibleToXML {
	String convertToXML(int tabLevel);
}
