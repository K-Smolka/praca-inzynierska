package xmlconverter;

import elements.Element;
import elements.ElementTag;
import elements.dataelements.*;
import elements.dataelements.simpledataelements.*;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import validation.ValidationData;
import validation.Validator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Krzysztof S. on 2017-08-19.
 */
class DataTypeFactory {

	static ElementWithData createDataTypeFromXMLNode(Node currentDataTypeNode) {
		String type = currentDataTypeNode.getAttributes().getNamedItem("type").getNodeValue();
		String name = null;

		try{
			name = currentDataTypeNode.getAttributes().getNamedItem("name").getNodeValue();
		}catch(NullPointerException ex) {/* normalna sytuacja, niektore nody moga nie miec nazwy, np FunctionCBox */}

		switch(ElementTag.valueOf(type)){
			case NUMERIC_VALUE:
				return createNumericValue(currentDataTypeNode);
			case POINT2D:
				return createPoint2d(currentDataTypeNode);
			case POINT3D:
				return createPoint3d(currentDataTypeNode);
			case STRING_VARIABLE:
				return createStringVariable(currentDataTypeNode);
			case GROUP:
				ElementWithSimpleData patternToGroup = getGroupableDataType(currentDataTypeNode);
				return new Group(name, patternToGroup, Integer.parseInt(currentDataTypeNode.getAttributes().getNamedItem("length").getNodeValue()));
			case FUNCTION:
				return new Function(name ,extractDataTypesFromTheList(currentDataTypeNode.getChildNodes()));
			case FUNCTION_CBOX:
				return createFunctionCBox(currentDataTypeNode);
			case CONST_VALUES_CBOX:
				return createConstValuesCBox(currentDataTypeNode);
			default:
				throw new IllegalArgumentException("Wrong type of DataType in XML, or type is not supported. " + type);
		}
	}

	private static ElementWithData createNumericValue(Node currentDataTypeNode) {
		String name = currentDataTypeNode.getAttributes().getNamedItem("name").getNodeValue();
		NumericValue result = new NumericValue(name);

		Map<Integer, List<ValidationData>> validations = extractValidationFromTheList(currentDataTypeNode.getChildNodes());

		if(!validations.isEmpty())
			result.numericDataField.setUpNewValidations(validations.get(1));

		return result;
	}

	private static ElementWithData createPoint2d(Node currentDataTypeNode) {
		String name = currentDataTypeNode.getAttributes().getNamedItem("name").getNodeValue();
		Point2d result = new Point2d(name);
		Map<Integer, List<ValidationData>> validations = extractValidationFromTheList(currentDataTypeNode.getChildNodes());

		if(!validations.isEmpty()){
			result.numericDataField1.setUpNewValidations(validations.get(1));
			result.numericDataField2.setUpNewValidations(validations.get(2));
		}

		return result;
	}

	private static ElementWithData createPoint3d(Node currentDataTypeNode) {
		String name = currentDataTypeNode.getAttributes().getNamedItem("name").getNodeValue();
		Point3d result = new Point3d(name);
		Map<Integer, List<ValidationData>> validations = extractValidationFromTheList(currentDataTypeNode.getChildNodes());

		if(!validations.isEmpty()){
			result.numericDataField1.setUpNewValidations(validations.get(1));
			result.numericDataField2.setUpNewValidations(validations.get(2));
			result.numericDataField3.setUpNewValidations(validations.get(3));
		}

		return result;
	}

	private static ElementWithData createStringVariable(Node currentDataTypeNode) {
		String name = currentDataTypeNode.getAttributes().getNamedItem("name").getNodeValue();
		StringVariable result = new StringVariable(name);
		Map<Integer, List<ValidationData>> validations = extractValidationFromTheList(currentDataTypeNode.getChildNodes());

		if(!validations.isEmpty())
			result.textDataField.setUpNewValidations(validations.get(1));

		return result;
	}

	private static List<ElementWithData> extractDataTypesFromTheList(NodeList listOfDataTypes) {
		ArrayList<ElementWithData> dataTypesFromCurrentXMLSegment = new ArrayList<>();
		Node currentDataTypeNode;
		ElementWithData currentDataType = null;
		for(int i=0; i<listOfDataTypes.getLength(); i++){
			if(listOfDataTypes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				if (("dataType".equals(listOfDataTypes.item(i).getNodeName()))) {
					currentDataTypeNode = listOfDataTypes.item(i);
					try {
						currentDataType = createDataTypeFromXMLNode(currentDataTypeNode);
					}catch (Exception ex){ System.out.println(ex.getMessage()); ex.printStackTrace();}

					dataTypesFromCurrentXMLSegment.add(currentDataType);
				}
			}
		}
		return dataTypesFromCurrentXMLSegment;
	}

	private static Map<Integer, List<ValidationData>> extractValidationFromTheList(NodeList listOfValidations) {
		Map<Integer, List<ValidationData>> validationsFromCurrentXMLSegment = new HashMap<>();
		Node currentValidationNode;
		for(int i=0; i<listOfValidations.getLength(); i++){
			if(listOfValidations.item(i).getNodeType() == Node.ELEMENT_NODE) {
				if (("validation".equals(listOfValidations.item(i).getNodeName()))) {
					if(validationsFromCurrentXMLSegment.isEmpty())
						validationsFromCurrentXMLSegment.put(1, new ArrayList<>());

					currentValidationNode = listOfValidations.item(i);
					Node inputFieldIndexNode = currentValidationNode.getAttributes().getNamedItem("inputFieldIndex");

					if(inputFieldIndexNode == null)
						validationsFromCurrentXMLSegment.get(1).add(extractValidationFromTheNode(currentValidationNode));
					else {
						Integer inputFieldNumber = Integer.parseInt(inputFieldIndexNode.getNodeValue());

						if(!validationsFromCurrentXMLSegment.containsKey(inputFieldNumber))
							validationsFromCurrentXMLSegment.put(inputFieldNumber, new ArrayList<>());

						validationsFromCurrentXMLSegment.get(inputFieldNumber).add(extractValidationFromTheNode(currentValidationNode));
					}
				}
			}
		}
		return validationsFromCurrentXMLSegment;
	}

	private static ValidationData extractValidationFromTheNode(Node validationNode) {
		String validationTypeString = validationNode.getAttributes().getNamedItem("type").getNodeValue();
		List<String> params = new ArrayList<>();

		Validator.ValidationTypes validationType = Validator.ValidationTypes.valueOf(validationTypeString);

		try{
			String currentParamValue;
			for (int paramCounter=0; paramCounter<validationType.getNumberOfParameters(); paramCounter++){
				currentParamValue = validationNode.getAttributes().getNamedItem("param" + (paramCounter+1)).getNodeValue();
				params.add(currentParamValue);
			}
		}catch (NullPointerException ex) {
			System.out.println("Problem with validation parameters in XML file!");
		}

		return new ValidationData(validationType, params);
	}

	/**
	 *
	 * @param currentDataTypeNode - Node DataType typu Group
	 * @return zwrca typ obslugiwany przez zadana grupe. Jesli zadany typ nie jest obslugiwany, to zwraca nulla.
	 */
	private static ElementWithSimpleData getGroupableDataType(Node currentDataTypeNode) {
		List<ElementWithData> extractedDataType = extractDataTypesFromTheList(currentDataTypeNode.getChildNodes());
		if(extractedDataType.size() != 1)
			throw new IllegalArgumentException("Wrong number of DataType elements in Group (XML)");

		Element resultDataType = extractedDataType.get(0);
		ElementTag resultClassTag = resultDataType.elementTag;

		if((resultClassTag != ElementTag.NUMERIC_VALUE) && (resultClassTag != ElementTag.POINT2D)
				&& (resultClassTag != ElementTag.POINT3D) && (resultClassTag != ElementTag.STRING_VARIABLE)) {
			throw new IllegalArgumentException("Not supported DataType in Group subNode (XML)");
		}

		return (ElementWithSimpleData) resultDataType;
	}

	private static FunctionCBox createFunctionCBox(Node currentDataTypeNode) {
		if(areAllChildrensFunctions(currentDataTypeNode)){
			// maly trik, zeby rzutowac List<DataType> na List<Function>:
			List<Function> functionsOfCBox = (List<Function>)(List<?>) extractDataTypesFromTheList(currentDataTypeNode.getChildNodes());
			return new FunctionCBox(functionsOfCBox);
		}
		throw new IllegalArgumentException("FunctionCBox moze miec tylko subNody typu Function! (XML)");
	}

	/**
	 *
	 * @param functionCBoxNode - Node typu FunctionCBox
	 * @return
	 */
	private static boolean areAllChildrensFunctions(Node functionCBoxNode) {
		NodeList listOfChildrens = functionCBoxNode.getChildNodes();
		for (int i = 0; i < listOfChildrens.getLength(); i++) {
			Node currentNode = listOfChildrens.item(i);
			if (currentNode.getNodeType() == Node.ELEMENT_NODE)
				if("dataType".equals(listOfChildrens.item(i).getNodeName())){
					if (!(ElementTag.FUNCTION.toString().equals(currentNode.getAttributes().getNamedItem("type").getNodeValue())))
						return false;
				}else
					return false;
		}
		return true;
	}

	private static ConstValuesCBox createConstValuesCBox(Node currentDataTypeNode) {
		String name = currentDataTypeNode.getAttributes().getNamedItem("name").getNodeValue();

		if(areAllChildrensConstValues(currentDataTypeNode)){
			List<String> constValuesOfCBox = extractConstValuesFromTheList(currentDataTypeNode.getChildNodes());
			return new ConstValuesCBox(name, constValuesOfCBox);
		}
		throw new IllegalArgumentException("ConstValuesCBox moze miec tylko subNody typu Function! (XML)");
	}

	private static List<String> extractConstValuesFromTheList(NodeList listOfDataTypes) {
		ArrayList<String> constValuesFromCurrentXMLSegment = new ArrayList<>();
		Node currentConstValueNode;
		String currentConstValue;
		for(int i=0; i<listOfDataTypes.getLength(); i++){
			if(listOfDataTypes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				if (("constValue".equals(listOfDataTypes.item(i).getNodeName()))) {
					currentConstValueNode = listOfDataTypes.item(i);
					currentConstValue = currentConstValueNode.getAttributes().getNamedItem("name").getNodeValue();
					constValuesFromCurrentXMLSegment.add(currentConstValue);
				}
			}
		}
		return constValuesFromCurrentXMLSegment;
	}

	/**
	 *
	 * @param constCBoxNode - Node typu ConstValuesCBox
	 * @return
	 */
	private static boolean areAllChildrensConstValues(Node constCBoxNode) {
		NodeList listOfChildrens = constCBoxNode.getChildNodes();
		for (int i = 0; i < listOfChildrens.getLength(); i++) {
			Node currentNode = listOfChildrens.item(i);
			if (currentNode.getNodeType() == Node.ELEMENT_NODE)
				if(!"constValue".equals(listOfChildrens.item(i).getNodeName()))
					return false;
		}
		return true;
	}
}
