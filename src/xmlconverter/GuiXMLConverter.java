package xmlconverter;

import application.FileElementsContainer;
import elements.CompleteFile;
import elements.Element;
import elements.FileSegment;
import elements.Row;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import windows.alertwindow.AlertWindow;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Krzysztof S. on 2017-07-16.
 */
public class GuiXMLConverter {

	public static Element importGuiFromXMLFile(File fXmlFile){

		DocumentBuilderFactory dbFactory;
		Document doc = null;
		DocumentBuilder dBuilder;

		try {
			dbFactory = DocumentBuilderFactory.newInstance();
			dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(fXmlFile);
		}catch(Exception e){
			new AlertWindow("Error during openining XML file").show();
			e.printStackTrace();
		}

		doc.getDocumentElement().normalize();

		String fileName =  doc.getDocumentElement().getAttribute("name");

		CompleteFile importedCompleteFile = new CompleteFile(fileName);

		NodeList nList = doc.getElementsByTagName("fileSegment");

		importedCompleteFile.addAllComponents(extractSegmentsFromNodeList(nList));

		return importedCompleteFile;
	}

	private static List<FileSegment> extractSegmentsFromNodeList(NodeList listOfSegments){
		ArrayList<FileSegment> segmentsFromXML = new ArrayList<>();
		Node currentSegmentNode;
		FileSegment currentSegment;
		for(int i=0; i<listOfSegments.getLength(); i++){
			currentSegmentNode = listOfSegments.item(i);
			currentSegment = new FileSegment(currentSegmentNode.getAttributes().getNamedItem("name").getNodeValue());

			NodeList rowsNodeList = currentSegmentNode.getChildNodes();
			currentSegment.addAllComponents(extractRowsFromNodeList(rowsNodeList));

			segmentsFromXML.add(currentSegment);
		}
		return segmentsFromXML;
	}

	private static List<Row> extractRowsFromNodeList(NodeList listOfRows){
		ArrayList<Row> rowsFromCurrentXMLSegment = new ArrayList<>();
		Node currentRowNode;
		Row currentRow;
		for(int i=0; i<listOfRows.getLength(); i++){
			if ("row".equals(listOfRows.item(i).getNodeName())) {
				currentRowNode = listOfRows.item(i);

				if(currentRowNode.getAttributes().getNamedItem("numberOfRows")== null){
					currentRow = new Row(rowsFromCurrentXMLSegment.size());
					NodeList variablesNodeList = currentRowNode.getChildNodes();
					currentRow.addAllComponents(extractDataTypesFromTheList(variablesNodeList));
					rowsFromCurrentXMLSegment.add(currentRow);
				}else{
					for(int j=0; j<Integer.parseInt(currentRowNode.getAttributes().getNamedItem("numberOfRows").getNodeValue()); j++){
						currentRow = new Row(rowsFromCurrentXMLSegment.size());
						NodeList variablesNodeList = currentRowNode.getChildNodes();
						currentRow.addAllComponents(extractDataTypesFromTheList(variablesNodeList));
						rowsFromCurrentXMLSegment.add(currentRow);
					}
				}
			}
		}
		return rowsFromCurrentXMLSegment;
	}

	private static List<Element> extractDataTypesFromTheList(NodeList listOfDataTypes) {
		ArrayList<Element> dataTypesFromCurrentXMLSegment = new ArrayList<>();
		Node currentDataTypeNode;
		Element currentDataType = null;
		for(int i=0; i<listOfDataTypes.getLength(); i++){
			if(listOfDataTypes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				if (("dataType".equals(listOfDataTypes.item(i).getNodeName()))) {
					currentDataTypeNode = listOfDataTypes.item(i);
					try {
						currentDataType = DataTypeFactory.createDataTypeFromXMLNode(currentDataTypeNode);
					}catch (Exception ex){ System.out.println(ex.getMessage()); ex.printStackTrace();}

					dataTypesFromCurrentXMLSegment.add(currentDataType);
				}
			}
		}
		return dataTypesFromCurrentXMLSegment;
	}

	public static void exportGUIToXMLFiles(File file){
		for(Element fileToExport: FileElementsContainer.files){
			try {
				PrintWriter writer = new PrintWriter(file.getPath() + ".\\" + fileToExport.getName() + ".xml", "UTF-8");
				writer.print(getFullFileContent(fileToExport));
				writer.close();
			} catch (FileNotFoundException e) {
				new AlertWindow("FileNotFoundException przy eksporcie GUI do pliku XML");
			} catch (UnsupportedEncodingException e) {
				new AlertWindow("UnsupportedEncodingException przy eksporcie GUI do pliku XML");
			}
		}
	}

	private static String getFullFileContent(Element fileToExport){
		return "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>" + fileToExport.convertToXML(0);
	}
}
