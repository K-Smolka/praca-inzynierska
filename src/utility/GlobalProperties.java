package utility;

/**
 * Created by Krzysztof S. on 2017-08-21.
 */
public class GlobalProperties {
	public static final double VERSION = 1.0;
	public static final int PRECISION_OF_CALCULATIONS = 20;
}
