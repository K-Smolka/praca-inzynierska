package utility;

import javafx.scene.control.TextField;
import validation.ValidationData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Krzysztof S. on 2017-08-18.
 */
public class TextFieldValidable extends TextField {
	private final List<ValidationData> validations = new ArrayList<>();

	public TextFieldValidable(List<ValidationData> validations) {
		this.validations.addAll(validations);
	}

	public TextFieldValidable(){
	}

	public List<ValidationData> getValidations() {
		return  Collections.unmodifiableList(validations);
	}

	public void removaAllValidations(){
		validations.clear();
	}

	public void addValidation(ValidationData newValidation){
		validations.add(newValidation);
	}

	public void addAllValidations(List<ValidationData> newValidations){
		for(ValidationData validation: newValidations)
			validations.add(validation);
	}

	public void setUpNewValidations(List<ValidationData> newValidations){
		validations.clear();
		for(ValidationData validation: newValidations)
			validations.add(validation);
	}



	/**
	 *
	 * @param fieldIndex index of TextField that is validated.
	 * @param tabLevel
	 * @return
	 */
	public String validationsToXML(int fieldIndex, int tabLevel){
		StringBuilder result = new StringBuilder();

		if(validations.isEmpty()){
			return "";
		}else{
			for (ValidationData validationData: validations) {
				result.append(System.lineSeparator() + getTabs(tabLevel));
				result.append("<validation inputFieldIndex=\"" +fieldIndex +"\" type=\"" + validationData.validationType + "\"");

				for (int paramCounter=0; paramCounter<validationData.validationParameters.size(); paramCounter++) {
					String validationParam = validationData.validationParameters.get(paramCounter);
					result.append(" param" + (paramCounter +1) + "=\"" + validationParam + "\"");
				}

				result.append(" />");
			}
		}

		return result.toString();
	}

	/**
	 * version for DataTypes with a single TextField.
	 */
	public String validationsToXML(int tabLevel){
		StringBuilder result = new StringBuilder();

		if(validations.isEmpty()){
			return "";
		}else{
			for (ValidationData validationData: validations) {
				result.append(System.lineSeparator() + getTabs(tabLevel));
				result.append("<validation type=\"" + validationData.validationType + "\"");

				for (int paramCounter=0; paramCounter<validationData.validationParameters.size(); paramCounter++) {
					String validationParam = validationData.validationParameters.get(paramCounter);
					result.append(" param" + (paramCounter +1) + "=\"" + validationParam + "\"");
				}

				result.append(" />");
			}
		}

		return result.toString();
	}

	protected String getTabs(int numberOfTabulators){
		String result = "";
		for(int i=0; i<numberOfTabulators; i++)
			result += "\t";

		return result;
	}

	public void setValidationStyle(boolean hasValidationPassed){
		if(hasValidationPassed)
			setStyle("");
		else
			setStyle("-fx-text-box-border: red ; -fx-focus-color: red ;");
	}
}
