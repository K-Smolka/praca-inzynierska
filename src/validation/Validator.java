package validation;

import elements.Element;
import utility.Expression;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Krzysztof S. on 2017-08-15.
 */
public class Validator {

	public enum ValidationTypes{
		IS_NOT_EMPTY,
		IS_NUMBER,
		IS_INTEGER,
		IS_NOT_NUMBER,
		CONTAINS_ANY_LETTERS,
		CONTAINS_ONLY_LETTERS,
		CONTAINS_GIVEN_CHARACTERS(1),
		IS_LOWER_THAN(1),
		IS_HIGHER_THAN(1),
		IS_LOWER_OR_EQUAL_TO(1),
		IS_HIGHER_OR_EQUAL_TO(1);

		private final int numberOfParameters;
		ValidationTypes(){numberOfParameters = 0;}
		ValidationTypes(int numberOfParameters){this.numberOfParameters = numberOfParameters;}

		public int getNumberOfParameters() {
			return numberOfParameters;
		}
	}

	/**
	 * Function is checking all pairs ValidationType-value from the map.
	 *
	 * @param validatedData
	 * @return
	 * If any of them do not pass the validation, function returns ValidationResult with validationStatus = false
	 */
	public static ValidationResult validate(ValidableData validatedData) {
		ValidationTypes validationType = validatedData.validationData.validationType;
		String validatedValue = validatedData.valueToValidate;
		List<String> validationParameters = validatedData.validationData.validationParameters;

		// break; = validation passed
		switch (validationType) {
			case IS_NOT_EMPTY:
				if (isNotEmpty(validatedValue))
					break;
				else
					return new ValidationResult(false, validationType, validatedValue);

			case IS_NUMBER:
				if (isNumber(validatedValue))
					break;
				else
					return new ValidationResult(false, validationType, validatedValue);

			case IS_INTEGER:
				if (isInteger(validatedValue))
					break;
				else
					return new ValidationResult(false, validationType, validatedValue);

			case IS_NOT_NUMBER:
				if (isNumber(validatedValue))
					return new ValidationResult(false, validationType, validatedValue);
				else
					break;

			case CONTAINS_ANY_LETTERS:
				if (containsAnyLetters(validatedValue))
					break;
				else
					return new ValidationResult(false, validationType, validatedValue);

			case CONTAINS_ONLY_LETTERS:
				if (containsOnlyLetters(validatedValue))
					break;
				else
					return new ValidationResult(false, validationType, validatedValue);

			case CONTAINS_GIVEN_CHARACTERS:
				if (containsGivenCharacters(validatedValue, validationParameters.get(0)))
					break;
				else
					return new ValidationResult(false, validationType, validatedValue,
							"Validation parameter: " + validationParameters.get(0));

			case IS_LOWER_THAN:
				if (isLowerThan(validatedValue, validationParameters.get(0)))
					break;
				else
					return new ValidationResult(false, validationType, validatedValue,
							"Validation parameter: " + validationParameters.get(0));

			case IS_HIGHER_THAN:
				if (isHigherThan(validatedValue, validationParameters.get(0)))
					break;
				else
					return new ValidationResult(false, validationType, validatedValue,
							"Validation parameter: " + validationParameters.get(0));

			case IS_LOWER_OR_EQUAL_TO:
				if (isLowerOrEqualTo(validatedValue, validationParameters.get(0)))
					break;
				else
					return new ValidationResult(false, validationType, validatedValue,
							"Validation parameter: " + validationParameters.get(0));

			case IS_HIGHER_OR_EQUAL_TO:
				if (isHigherOrEqualTo(validatedValue, validationParameters.get(0)))
					break;
				else
					return new ValidationResult(false, validationType, validatedValue,
							"Validation parameter: " + validationParameters.get(0));
		}

		return new ValidationResult(true);
	}

	private static boolean isNotEmpty(String validatedValue){
		if (validatedValue == null)
			return false;
		else if (validatedValue.equals(""))
			return false;
		else
			return true;
	}

	private static boolean isNumber(String validatedValue){
		if("".equals(validatedValue))
			return true;

		try{
			BigDecimal bd = new Expression(validatedValue).eval();
		}
		catch(Exception e){
			return false;
		}

		return true;
	}

	private static boolean isInteger(String validatedValue){
		if("".equals(validatedValue))
			return true;

		try{
			Integer.parseInt(validatedValue);
		}
		catch(Exception e){
			return false;
		}

		return true;
	}

	private static boolean containsAnyLetters(String validatedValue) {
		if("".equals(validatedValue))
			return true;

		return validatedValue.matches(".*[a-zA-Z].*");
	}

	private static boolean containsOnlyLetters(String validatedValue) {
		if("".equals(validatedValue))
			return true;

		for (Character c: validatedValue.toCharArray()) {
			if(c.toString().matches("[a-zA-Z]"))
				continue;
			else
				return false;
		}
		return true;
	}

	private static boolean containsGivenCharacters(String validatedValue, String givenCharacters) {
		if("".equals(validatedValue))
			return true;

		if(validatedValue.contains(givenCharacters))
			return true;
		else
			return false;
	}

	private static boolean isLowerThan(String validatedValue, String valueToCompare) {
		if("".equals(validatedValue))
			return true;

		if(!isNumber(validatedValue) || !isNumber(valueToCompare))
			return false;

		BigDecimal val = new Expression(validatedValue).eval();
		BigDecimal valToCompare = new Expression(valueToCompare).eval();

		return (val.compareTo(valToCompare) <0) ? true : false;
	}

	private static boolean isHigherThan(String validatedValue, String valueToCompare) {
		if("".equals(validatedValue))
			return true;

		if(!isNumber(validatedValue) || !isNumber(valueToCompare))
			return false;

		BigDecimal val = new Expression(validatedValue).eval();
		BigDecimal valToCompare = new Expression(valueToCompare).eval();

		return (val.compareTo(valToCompare) >0) ? true : false;
	}

	private static boolean isLowerOrEqualTo(String validatedValue, String valueToCompare) {
		if("".equals(validatedValue))
			return true;

		if(!isNumber(validatedValue) || !isNumber(valueToCompare))
			return false;

		BigDecimal val = new Expression(validatedValue).eval();
		BigDecimal valToCompare = new Expression(valueToCompare).eval();

		return (val.compareTo(valToCompare) <=0) ? true : false;
	}

	private static boolean isHigherOrEqualTo(String validatedValue, String valueToCompare) {
		if("".equals(validatedValue))
			return true;

		if(!isNumber(validatedValue) || !isNumber(valueToCompare))
			return false;

		BigDecimal val = new Expression(validatedValue).eval();
		BigDecimal valToCompare = new Expression(valueToCompare).eval();

		return (val.compareTo(valToCompare) >=0) ? true : false;
	}

	/**
	 * Validate whole file.
	 * @param fileToValidate
	 * @return Returns all ValidationResults that failed; else, return positive ValidationResult.
	 */
	public static List<ValidationResult> validateFile(Element fileToValidate){
		List<ValidationResult> validationResults = fileToValidate.validate();
		List<ValidationResult> failedValidationResults = new ArrayList<>();

		for (ValidationResult validationResult: validationResults)
			if(!validationResult.hasValidationPassed())
				failedValidationResults.add(validationResult);

		if(failedValidationResults.isEmpty())
			return new ArrayList<ValidationResult>(){{add(new ValidationResult(true));}};
		else
			return failedValidationResults;
	}
}

