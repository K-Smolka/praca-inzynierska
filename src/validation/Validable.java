package validation;

import java.util.List;

/**
 * Created by Krzysztof S. on 2017-08-15.
 */
public interface Validable {
	List<ValidationResult> validate();
}
