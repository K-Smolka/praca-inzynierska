package validation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Krzysztof S. on 2017-08-17.
 *
 * Contains all the data about validation. Its type and parameters.
 */
public class ValidationData {
	public final Validator.ValidationTypes validationType;
	public List<String> validationParameters;

	public ValidationData(Validator.ValidationTypes validationType, List<String> validationParameters) {
		this.validationType = validationType;
		this.validationParameters = (validationParameters != null) ? validationParameters : new ArrayList<>();
	}

	@Override
	public String toString(){
		return validationType.toString();
	}
}
