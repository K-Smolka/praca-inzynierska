package validation;

/**
 * Created by Krzysztof S. on 2017-08-15.
 *
 * Contains all the data needed to process validation.
 */
public final class ValidableData {
	public final String valueToValidate;
	public final ValidationData validationData;

	public ValidableData(String valueToValidate, ValidationData validationData) {
		this.valueToValidate = valueToValidate;
		this.validationData = validationData;
	}
}
