package validation;

/**
 * Created by Krzysztof S. on 2017-08-15.
 *
 * <br/><br/>Class is immutable.
 */

public final class ValidationResult {
	private final boolean validatedCorrectly;

	/**
	 * nullable
	 */
	private final Validator.ValidationTypes failedValidationType;

	/**
	 * nullable
	 */
	private final String valueThatFailedValidation;

	private final String additionalMessage;

	/**
	 * Call this constructor if validation passed.
	 * @param validatedCorrectly
	 */
	public ValidationResult(boolean validatedCorrectly){
		this.validatedCorrectly = validatedCorrectly;
		failedValidationType = null;
		valueThatFailedValidation = null;
		additionalMessage = null;
	}

	public ValidationResult(boolean validatedCorrectly, Validator.ValidationTypes failedValidationType, String valueThatFailedValidation){
		this.validatedCorrectly = validatedCorrectly;
		this.failedValidationType = failedValidationType;
		this.valueThatFailedValidation = valueThatFailedValidation;
		additionalMessage = null;
	}

	public ValidationResult(boolean validatedCorrectly, Validator.ValidationTypes failedValidationType, String valueThatFailedValidation, String additionalMessage) {
		this.validatedCorrectly = validatedCorrectly;
		this.failedValidationType = failedValidationType;
		this.valueThatFailedValidation = valueThatFailedValidation;
		this.additionalMessage = additionalMessage;
	}

	public boolean hasValidationPassed(){
		return validatedCorrectly;
	}

	public Validator.ValidationTypes getFailedValidationType() {
		return failedValidationType;
	}

	public String getValueThatFailedValidation() {
		return valueThatFailedValidation;
	}

	public String getValidationInfoMessage() {
		String result;

		if(validatedCorrectly)
			result = "Validation passed correctly. No errors.";
		else
			result = "Validation that failed: " + failedValidationType.toString() + " for value: " + valueThatFailedValidation;

		if(additionalMessage != null && !"".equals(additionalMessage))
			result += System.lineSeparator() + "\t" + additionalMessage;

		return result;
	}
}
