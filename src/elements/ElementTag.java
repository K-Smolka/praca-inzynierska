package elements;

/**
 * Created by Krzysztof S. on 2017-09-01.
 */
public enum ElementTag{
		FILE,
		SEGMENT,
		ROW,
		NUMERIC_VALUE(true, true),
		POINT2D(true, true),
		POINT3D(true, true),
		STRING_VARIABLE(true, true),
		CONST_VALUES_CBOX(true, true),
		GROUP(true, false),
		FUNCTION(true, false),
		FUNCTION_CBOX(true, false);

	public final boolean containsRawData;
	public final boolean isSingleDataElement;

	ElementTag(){
		this.containsRawData = false;
		this.isSingleDataElement = false;
	}

	ElementTag(boolean containsRawData, boolean isSingleDataElement){
		this.containsRawData = containsRawData;
		this.isSingleDataElement = isSingleDataElement;
	}
}
