package elements;

import com.sun.javafx.tk.FontLoader;
import com.sun.javafx.tk.Toolkit;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 * Created by Krzysztof S. on 2017-09-01.
 */
public class CompleteFile extends Element{

	public CompleteFile(String fileName) {
		super(ElementTag.FILE, fileName);

		layout = new VBox();
		layout.setPadding(new Insets(5, 0, 0, 5));

		labelName = new Label(name);
		centerLabelNameInLayout();
		labelName.setStyle("-fx-font-weight: bold;");

		refreshLayout();
	}

	@Override
	public Pane refreshLayout() {
		layout.getChildren().clear();
		layout.getChildren().add(labelName);
		layout.getChildren().add(getAllComponentsPane());

		return layout;
	}

	@Override
	protected boolean validateComponentTag(Element newComponent) {
		return newComponent.elementTag.equals(ElementTag.SEGMENT);
	}

	@Override
	public String getDataString(int tabLevel) {
		String result = "";
		for (Element component : components) {
			result += component.getDataString(tabLevel);
			result += System.lineSeparator() + System.lineSeparator();
		}

		return result;
	}

	@Override
	public String convertToXML(int tabLevel){
		StringBuilder result = new StringBuilder();

		result.append(System.lineSeparator() + "<file name=\""+ getName() +"\">");

		for(Element segment: components)
			result.append(segment.convertToXML(tabLevel+1));

		result.append(System.lineSeparator() + "</file>");

		return result.toString();
	}

	@Override
	public void setName(String newName) {
		name = newName;
		labelName.setText(name);
		centerLabelNameInLayout();
	}

	private void centerLabelNameInLayout() {
		FontLoader fontLoader = Toolkit.getToolkit().getFontLoader();
		double labelContentWidth = fontLoader.computeStringWidth(labelName.getText(), labelName.fontProperty().get());
		labelName.setPadding(new Insets(0, 0, 0, 170-labelContentWidth/2));
	}

}
