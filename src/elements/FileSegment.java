package elements;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Krzysztof S. on 2017-09-01.
 */
public class FileSegment extends Element{

	public FileSegment(String segmentName) {
		super(ElementTag.SEGMENT, segmentName);

		VBox layout = new VBox();
		layout.setPadding(new Insets(5, 0, 0, 5));
		layout.setSpacing(0);
		this.layout = layout;

		labelName = new Label(name);

		refreshLayout();
	}

	@Override
	public Pane refreshLayout() {
		layout.getChildren().clear();
		layout.getChildren().add(labelName);

		for (Element row : components) {
			layout.getChildren().add(row.getFullLayout());
		}

		return layout;
	}

	@Override
	protected boolean validateComponentTag(Element newComponent) {
		return newComponent.elementTag.equals(ElementTag.ROW);
	}

	public String getDataString(int tabLevel){
		String result = this.getTabs(tabLevel) + this.getName() + ":" + System.lineSeparator() + this.getTabs(tabLevel) + "(" + System.lineSeparator();

		List<String> rows = components.stream().map(row -> row.getDataString(tabLevel+1)).collect(Collectors.toList());

		result += String.join(", " + System.lineSeparator(), rows);
		result += System.lineSeparator() + this.getTabs(tabLevel) + ");";

		return result;
	}

	@Override
	public String convertToXML(int tabLevel){
		StringBuilder result = new StringBuilder();

		result.append(System.lineSeparator() + getTabs(tabLevel));
		result.append("<fileSegment name=\""+ getName() +"\">");

		if(!components.isEmpty())
			if (areAllRowsContentTheSame())	//zapisywanie rowow z atrybutem numberOfRows
				result.append(((Row)components.get(0)).convertToXMLWithNumberOfRowsAttribute(tabLevel+1, components.size()));
			else
				for(Element row: components)			// normalne zapisywanie
					result.append(row.convertToXML(tabLevel+1));

		result.append(System.lineSeparator() + getTabs(tabLevel));
		result.append("</fileSegment>");

		return result.toString();
	}

	public int getRowNumberForNewRow(){
		if(!components.isEmpty())
			return ((Row)components.get(components.size()-1)).rowNumber+1;
		else
			return 0;
	}

	private boolean areAllRowsContentTheSame() {
		Element sampleRow = components.get(0);

		for(Element row: components){
			if(!row.convertToXML(0).equals(sampleRow.convertToXML(0)))
				return false;
		}
		return true;
	}
}
