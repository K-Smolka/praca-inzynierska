package elements;

import javafx.geometry.Insets;
import javafx.scene.control.Separator;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 * Created by Krzysztof S. on 2017-09-01.
 */
public class Row extends Element{

	public int rowNumber;

	public Row(int rowNumber){
		super(ElementTag.ROW);

		this.rowNumber = rowNumber;
		this.name = "#" + (this.rowNumber+1);
		this.treeItemHandler = getTreeItemHandler();

		VBox layout = new VBox();
		layout.setPadding(new Insets(5, 0, 0, 5));
		layout.setSpacing(0);
		this.layout = layout;

		refreshLayout();
	}

	@Override
	public Pane refreshLayout() {
		layout.getChildren().clear();
		layout.getChildren().add(new Separator());

		if(rowNumber%2 == 1)
			setBackgroundColor("#eeeeee");
		else
			setBackgroundColor(null);

		for (Element variable : components)
			layout.getChildren().add(variable.getFullLayout());

		return layout;
	}

	@Override
	protected boolean validateComponentTag(Element newComponent) {
		return newComponent.elementTag.containsRawData;
	}

	/**
	 *
	 * @param color
	 * css notation of color, e.g. #eeeeee
	 */
	private void setBackgroundColor(String color){
		if((color != null) && !("".equals(color)) )
			for(Element component: components)
				component.getFullLayout().setStyle("-fx-background-color: "+ color +";");
		else
			layout.setStyle(null);
	}

	public String getDataString(int tabLevel){
		StringBuilder result = new StringBuilder(this.getTabs(tabLevel) + "{" + System.lineSeparator());
		for(Element dataElement: components)
			result.append(dataElement.getDataString(tabLevel+1) + System.lineSeparator());

		result.append(this.getTabs(tabLevel) + "}");
		return result.toString();
	}

	@Override
	public String convertToXML(int tabLevel){
		StringBuilder result = new StringBuilder();

		result.append(System.lineSeparator() + getTabs(tabLevel));
		result.append("<row>");

		for(Element variable: components)
			result.append(variable.convertToXML(tabLevel+1));

		result.append(System.lineSeparator() + getTabs(tabLevel));
		result.append("</row>");

		return result.toString();
	}

	//SPRAWDZIC ALBO PRZEROBIC, TAK BY NIE TRZEBA BYLO JEJ WYWOLYWAC OSOBNO!
	/**
	 * Fonkcja jest zamiennikiem convertToXML(...). Zwraca element row w xml w skroconej notacji. Opisuje zawartosc
	 * jednego wiersza, z atrybutem mowiacym ile razy ten wiersz ma zostac powtorzony. Jest używana przy generowaniu xml
	 * wyłącznie gdy wszystkie wiersze segmentu sa identyczne.
	 *
	 * @param tabLevel
	 * @param numberOfRows - ile razy wiersz ma byc powtorzony
	 * @return
	 */
	public String convertToXMLWithNumberOfRowsAttribute(int tabLevel, int numberOfRows){
		StringBuilder result = new StringBuilder();

		result.append(System.lineSeparator() + getTabs(tabLevel));
		result.append("<row numberOfRows=\"" + numberOfRows + "\">");

		for(Element variable: components)
			result.append(variable.convertToXML(tabLevel+1));

		result.append(System.lineSeparator() + getTabs(tabLevel));
		result.append("</row>");

		return result.toString();
	}
}
