package elements.dataelements.simpledataelements;

import elements.Element;
import elements.ElementTag;
import elements.dataelements.ElementWithData;
import javafx.scene.layout.Pane;
import utility.Expression;
import utility.GlobalProperties;
import utility.TextFieldValidable;
import validation.ValidableData;
import validation.ValidationData;
import validation.ValidationResult;
import validation.Validator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Krzysztof S. on 2017-09-01.
 */
public abstract class ElementWithSimpleData extends ElementWithData {

	/**
	 * Utility dataFields-handler. Contains all data fields of element.
	 */
	protected List<TextFieldValidable> dataFields;

	protected String defaultFieldsValue;

	public ElementWithSimpleData(ElementTag elementTag, String elementName) {
		super(elementTag, elementName);
		dataFields = new ArrayList<>();
	}

	@Override
	protected boolean validateComponentTag(Element newComponent) {
		return false;
	}

	@Override
	public Pane refreshLayout() {
		leftSideDataLayout.getChildren().clear();
		rightSideDataLayout.getChildren().clear();

		if(!dataFields.isEmpty()) {
			leftSideDataLayout.getChildren().add(labelName);

			for (TextFieldValidable dataField : dataFields)
				rightSideDataLayout.getChildren().add(dataField);
		}

		layout.getChildren().clear();
		layout.getChildren().add(leftSideDataLayout);
		layout.getChildren().add(rightSideDataLayout);

		for (Element component: components)
			layout.getChildren().add(component.getFullLayout());

		return layout;
	}

	protected void addField(TextFieldValidable newElement){
		dataFields.add(newElement);
	}

	//-- VALIDATION: --//

	@Override
	public List<ValidationResult> validate() {
		List<ValidationResult> resultsList = new ArrayList<>();

		for (TextFieldValidable fieldToValidate :dataFields) {
			List<ValidationResult> resultsListOfSingleField = validateInputField(fieldToValidate);
			setGUIInfoAboutValidation(fieldToValidate ,resultsListOfSingleField);
			resultsList.addAll(resultsListOfSingleField);
		}

		return resultsList;
	}

	private List<ValidationResult> validateInputField(TextFieldValidable fieldToValidate) {
		List<ValidationResult> resultList = new ArrayList<>();

		for (ValidationData validation: fieldToValidate.getValidations())
			resultList.add(Validator.validate(new ValidableData(fieldToValidate.getText(), validation)));

		return resultList;
	}

	private void setGUIInfoAboutValidation(TextFieldValidable validatedTextField ,List<ValidationResult> resultList) {
		boolean allValidationsPassed = true;

		for (ValidationResult validationResult: resultList)
			if(!validationResult.hasValidationPassed()) {
				allValidationsPassed = false;
				break;
			}

		validatedTextField.setValidationStyle(allValidationsPassed);
	}

	public void addNewValidation(ValidationData newValidation, TextFieldValidable dataField) {
		dataField.addValidation(newValidation);
	}

	public void setUpNewValidations(List<ValidationData> newValidationsList, TextFieldValidable dataField) {
			dataField.removaAllValidations();
			for (ValidationData validation : newValidationsList)
				dataField.addValidation(validation);
	}

	protected String calculateExpression(String expression){
		String result = null;
		try{
			Expression resultOfExpression = new Expression(expression);
			resultOfExpression.setPrecision(GlobalProperties.PRECISION_OF_CALCULATIONS);

			result = resultOfExpression.eval().toString();
			if(!result.contains("."))
				result += ".0";
		}catch(Exception ex){
			System.out.println("Blad przy eksporcie! Zly format liczby");
			return null;
		}
		return result;
	}

	@Override
	public String getDataString(int tabLevel) {
		List<String> values = new ArrayList<>();

		for (TextFieldValidable dataField : dataFields)
			if (dataField.getText().isEmpty())
				values.add(defaultFieldsValue);
			else
				if(calculateExpression(dataField.getText()) != null)
					values.add(calculateExpression(dataField.getText()));
				else
					values.add(dataField.getText());

		String result;
		if(values.size()==1)
			result = getTabs(tabLevel) + this.getName() + " = \"" + values.get(0) + "\";";
		else{
			result = getTabs(tabLevel) + this.getName() + " = [";
			result += String.join(", ", values);
			result += "];";
		}

		return result;
	}

	/**
	 * @return dataString without name, braces and semicolons.
	 */
	public String getUnwrappedDataString() {
		List<String> values = new ArrayList<>();

		for (TextFieldValidable dataField : dataFields)
			if (dataField.getText().isEmpty())
				values.add(defaultFieldsValue);
			else
				values.add(calculateExpression(dataField.getText()));

		return String.join(", ", values);
	}

	@Override
	public String convertToXML(int tabLevel){
		StringBuilder result = new StringBuilder();

		result.append(System.lineSeparator() + getTabs(tabLevel));

		if(components.isEmpty() && !hasInputFieldsAnyValidation()){
			result.append("<dataType name=\"" + getName() + "\" type=\"" + this.elementTag + "\" />");
		}else{
			result.append("<dataType name=\"" + getName() + "\" type=\"" + this.elementTag + "\" >");

			for(int fieldIndex=0; fieldIndex<dataFields.size(); fieldIndex++){
				TextFieldValidable fieldToExport = dataFields.get(fieldIndex);
				if(dataFields.size() > 1)
					result.append(fieldToExport.validationsToXML(fieldIndex+1,tabLevel+1)); //fieldIndex jest w xmlu numerowany od 1 a nie od 0.
				else
					result.append(fieldToExport.validationsToXML(tabLevel+1));
			}

			for(Element component: components)
				result.append(component.convertToXML(tabLevel+1));

			result.append(System.lineSeparator() + getTabs(tabLevel));
			result.append("</dataType>");
		}

		return result.toString();
	}

	private boolean hasInputFieldsAnyValidation(){
		for(TextFieldValidable dataField: dataFields){
			if(!dataField.getValidations().isEmpty())
				return true;
		}
		return false;
	}
}
