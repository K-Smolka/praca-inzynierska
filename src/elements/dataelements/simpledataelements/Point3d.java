package elements.dataelements.simpledataelements;

import elements.Element;
import elements.ElementTag;
import utility.TextFieldValidable;
import validation.ValidationData;
import validation.Validator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Krzysztof S. on 2017-09-01.
 */
public class Point3d extends ElementWithSimpleData {

	public TextFieldValidable numericDataField1;
	public TextFieldValidable numericDataField2;
	public TextFieldValidable numericDataField3;

	public Point3d(String elementName) {
		super(ElementTag.POINT3D, elementName);
		setUpVariableName(elementName);
		defaultFieldsValue = "0.0";

		numericDataField1 = new TextFieldValidable();
		numericDataField1.setPrefWidth(50);
		numericDataField1.setPromptText(defaultFieldsValue);
		numericDataField1.textProperty().addListener((observable, oldValue, newValue) -> validate());

		numericDataField2 = new TextFieldValidable();
		numericDataField2.setPrefWidth(50);
		numericDataField2.setPromptText(defaultFieldsValue);
		numericDataField2.textProperty().addListener((observable, oldValue, newValue) -> validate());

		numericDataField3 = new TextFieldValidable();
		numericDataField3.setPrefWidth(50);
		numericDataField3.setPromptText(defaultFieldsValue);
		numericDataField3.textProperty().addListener((observable, oldValue, newValue) -> validate());

		this.addField(numericDataField1);
		this.addField(numericDataField2);
		this.addField(numericDataField3);
		refreshLayout();
	}

	public Point3d(Point3d toClone) {
		this(toClone.getName());
		setUpNewValidations(toClone.numericDataField1.getValidations(), numericDataField1);
		setUpNewValidations(toClone.numericDataField2.getValidations(), numericDataField2);
		setUpNewValidations(toClone.numericDataField3.getValidations(), numericDataField3);
	}

	@Override
	protected boolean validateComponentTag(Element newComponent) {
		return false;
	}

	@Override
	public String getDataString(int tabLevel) {
		List<String> values = new ArrayList<>();

		for (TextFieldValidable dataField : dataFields)
			if (dataField.getText().isEmpty())
				values.add(defaultFieldsValue);
			else
				values.add(calculateExpression(dataField.getText()));

		return getTabs(tabLevel) + this.getName() + " = [" + String.join(", ", values) + "];";
	}
}
