package elements.dataelements.simpledataelements;

import elements.Element;
import elements.ElementTag;
import utility.TextFieldValidable;
import validation.ValidationData;
import validation.Validator;

/**
 * Created by Krzysztof S. on 2017-09-01.
 */
public class NumericValue extends ElementWithSimpleData {

	public TextFieldValidable numericDataField;

	public NumericValue(String elementName) {
		super(ElementTag.NUMERIC_VALUE, elementName);
		setUpVariableName(elementName);
		defaultFieldsValue = "0.0";

		numericDataField = new TextFieldValidable();
		numericDataField.setPrefWidth(50);
		numericDataField.setPromptText(defaultFieldsValue);
		numericDataField.textProperty().addListener((observable, oldValue, newValue) -> validate());

		this.addField(numericDataField);
		refreshLayout();
	}

	public NumericValue(NumericValue toClone) {
		this(toClone.getName());

		setUpNewValidations(toClone.numericDataField.getValidations(), numericDataField);
	}

	@Override
	protected boolean validateComponentTag(Element newComponent) {
		return false;
	}

	@Override
	public String getDataString(int tabLevel) {
		String value;

		if (numericDataField.getText().isEmpty())
			value = defaultFieldsValue;
		else
			value = calculateExpression(numericDataField.getText());

		return getTabs(tabLevel) + this.getName() + " = " + value + ";";
	}
}
