package elements.dataelements.simpledataelements;

import elements.Element;
import elements.ElementTag;
import utility.TextFieldValidable;

/**
 * Created by Krzysztof S. on 2017-09-01.
 */
public class StringVariable extends ElementWithSimpleData {

	public TextFieldValidable textDataField;

	public StringVariable(String elementName) {
		super(ElementTag.STRING_VARIABLE, elementName);
		setUpVariableName(elementName);
		defaultFieldsValue = "";

		textDataField = new TextFieldValidable();
		textDataField.setPrefWidth(50);
		textDataField.setPromptText("Text");
		textDataField.textProperty().addListener((observable, oldValue, newValue) -> validate());

		this.addField(textDataField);
		refreshLayout();
	}

	public StringVariable(StringVariable toClone) {
		this(toClone.getName());
		setUpNewValidations(toClone.textDataField.getValidations(), textDataField);
	}

	@Override
	protected boolean validateComponentTag(Element newComponent) {
		return false;
	}

	@Override
	public String getDataString(int tabLevel) {
		String value;
		if (textDataField.getText().isEmpty())
			value = defaultFieldsValue;
		else
			value = textDataField.getText();

		return getTabs(tabLevel) + this.getName() + " = \"" + value + "\";";
	}

}
