package elements.dataelements;

import elements.Element;
import elements.ElementTag;
import elements.dataelements.simpledataelements.NumericValue;
import elements.dataelements.simpledataelements.Point2d;
import elements.dataelements.simpledataelements.Point3d;
import elements.dataelements.simpledataelements.StringVariable;
import javafx.scene.layout.VBox;

import java.util.List;

/**
 * Created by Krzysztof S. on 2017-09-01.
 */
public class Function extends ElementWithData{

	/**
	 *
	 * @param functionName - oznacza nazwe funkcji wyswietlana w FunctionCBox. Jest przypisywana do variableName.
	 * @param functionFields
	 */
	public Function(String functionName, List<ElementWithData> functionFields) {
		super(ElementTag.FUNCTION, functionName);
		setUpVariableName(functionName);
		layout = new VBox();
		components.addAll(functionFields);

		refreshLayout();
	}

	public Function(Function toClone){
		super(ElementTag.FUNCTION, toClone.getName());
		layout = new VBox();

		for(Element functionElement: toClone.components){
			components.add(cloneDataType(functionElement));
		}

		refreshLayout();
	}


	public Element cloneDataType(Element pattern){
		Element result;
		switch(pattern.elementTag){
			case NUMERIC_VALUE:
				result = new NumericValue((NumericValue) pattern);
				break;
			case POINT2D:
				result = new Point2d((Point2d) pattern);
				break;
			case POINT3D:
				result = new Point3d((Point3d) pattern);
				break;
			case STRING_VARIABLE:
				result = new StringVariable((StringVariable) pattern);
				break;
			case GROUP:
				result = new Group((Group) pattern);
				break;
			case CONST_VALUES_CBOX:
				result = new ConstValuesCBox((ConstValuesCBox) pattern);
				break;
			case FUNCTION_CBOX:
				result = new FunctionCBox((FunctionCBox) pattern);
				break;
			default:
				throw new IllegalArgumentException("Type is not groupable!");
		}

		return result;
	}

	@Override
	protected boolean validateComponentTag(Element newComponent) {
		return false;
	}

	@Override
	public String toString(){
		return getName();
	}

	@Override
	public String getDataString(int tabLevel) {
		String result = getTabs(tabLevel) + this.getName() + ":{" + System.lineSeparator();

		for(Element component: components)
			result += component.getDataString(tabLevel+1) + System.lineSeparator();

		result += getTabs(tabLevel) + "};";
		return result;
	}

	@Override
	public String convertToXML(int tabLevel){
		StringBuilder result = new StringBuilder();

		result.append(System.lineSeparator() + getTabs(tabLevel));

		if(components.isEmpty()){
			result.append("<dataType name=\"" + getName() + "\" type=\"" + this.elementTag + "\" />");
		}else {
			result.append("<dataType name=\"" + getName() + "\" type=\"" + this.elementTag + "\" >");

			for(Element variable: components)
				result.append(variable.convertToXML(tabLevel+1));

			result.append(System.lineSeparator() + getTabs(tabLevel));
			result.append("</dataType>");
		}

		return result.toString();
	}
}
