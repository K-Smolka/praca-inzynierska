package elements.dataelements;

import elements.Element;
import elements.ElementTag;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import treeviewcustom.treeitems.AbstractTreeItem;
import treeviewcustom.treeitems.TreeItemsBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Krzysztof S. on 2017-09-01.
 */
public class FunctionCBox extends ElementWithData{

	/**
	 * functions - przechowuje obslugiwane funkcje
	 */
	public List<Function> functions;

	/**
	 * selectedFunction - obecnie wybrana funkcja, nalezy ja ustawiac wylacznie przez selectNewFunction().
	 */
	public Function selectedFunction;

	private ComboBox<String> comboBox;

	public FunctionCBox(List<Function> functions) {
		super(ElementTag.FUNCTION_CBOX, "");
		this.functions = functions;

		initializeLayout();

		refreshLayout();
	}

	public FunctionCBox(FunctionCBox toCopy) {
		super(ElementTag.FUNCTION_CBOX, "");

		functions = new ArrayList<>();
		for (Function function: toCopy.functions)
			functions.add(new Function(function));

		initializeLayout();

		refreshLayout();
	}

	private void initializeLayout() {
		layout = new VBox();
		layout.setPadding(new Insets(0, 10, 5, 0));

		ObservableList<String> cBoxOptions = FXCollections.observableArrayList();

		for (Function f: functions)
			cBoxOptions.add(f.getName());

		comboBox = new ComboBox(cBoxOptions);
		comboBox.setValue(cBoxOptions.get(0));
		comboBox.setMinWidth(150);
		comboBox.setMaxWidth(150);

		comboBox.valueProperty().addListener(new ChangeListener<String>() {
			@Override public void changed(ObservableValue observedValue, String oldValue, String newValue) {
				selectNewFunction(newValue);
				refreshTreeItemHandler();
			}
		});

		if(!this.functions.isEmpty())
			selectNewFunction(functions.get(0));
	}

	public void removeAllFunctions(){
		this.functions.clear();
		comboBox.getItems().clear();
	}

	public void addNewFunction(Function newFunction){
		this.functions.add(newFunction);
		comboBox.getItems().add(newFunction.getName());

		if(comboBox.getItems().size() == 1)
			comboBox.getSelectionModel().select(0);
	}

	private void selectNewFunction(String newFunctionName){
		if(functions.isEmpty() || newFunctionName == null || "".equals(newFunctionName))
			return;

		for (Function function: functions)
			if(function.getName().equals(newFunctionName)) {
				selectNewFunction(function);
				return;
			}

		throw new IllegalArgumentException("Funkcja nie jest obslugiwana.");
	}

	private void selectNewFunction(Function newFunction){
		selectedFunction = newFunction;
		components.clear();

		for(Element newParameter : newFunction.components)
			addFunctionParameter((ElementWithData)newParameter);

		refreshLayout();
	}

	public void addFunctionParameter(ElementWithData newFunctionParameter){
		components.add(newFunctionParameter);
//		refreshLayout(); ???
	}

	@Override
	protected boolean validateComponentTag(Element newComponent) {
		return false;
	}

	@Override
	public AbstractTreeItem getTreeItemHandler(){
		if(functions == null || functions.isEmpty())
			return null;

		if(treeItemHandler == null){
			treeItemHandler = TreeItemsBuilder.createTreeItem(this.selectedFunction.getName(), ID, ElementTag.FUNCTION_CBOX);
			for(Element component: components)
				treeItemHandler.getChildren().add(component.getTreeItemHandler());
		}

		return treeItemHandler;
	}

	private void refreshTreeItemHandler(){
		treeItemHandler.getChildren().clear();
		treeItemHandler.setValue(this.selectedFunction.getName());

		for(Element component: components)
			treeItemHandler.getChildren().add(component.getTreeItemHandler());
	}

	public void refreshComboBox(){
		comboBox.getItems().clear();
		for (Function f: functions)
			comboBox.getItems().add(f.getName());

		if(comboBox.getItems().contains(selectedFunction.getName()))
			comboBox.getSelectionModel().select(selectedFunction.getName());
		else
			comboBox.getSelectionModel().select(0);
	}

	@Override
	public Pane refreshLayout(){
		if(leftSideDataLayout.getChildren().isEmpty()) {
			leftSideDataLayout.getChildren().add(comboBox);
			layout.getChildren().add(leftSideDataLayout);
		}

		// dzieki temu combobox sie nie blokuje:
		if(layout.getChildren().size() > 1)
			layout.getChildren().remove(1, layout.getChildren().size());


		for (Element component: components)
			layout.getChildren().add(component.getFullLayout());

		return layout;
	}

	@Override
	public String getDataString(int tabLevel) {
		return selectedFunction.getDataString(tabLevel);
	}

	@Override
	public String convertToXML(int tabLevel){
		StringBuilder result = new StringBuilder();

		result.append(System.lineSeparator() + getTabs(tabLevel));

		if(functions.isEmpty()){
			result.append("<dataType type=\"" + this.elementTag + "\" />");
		}else{
			result.append("<dataType type=\"" + this.elementTag + "\" >");

			for(Function function: functions)
				result.append(function.convertToXML(tabLevel + 1));

			result.append(System.lineSeparator() + getTabs(tabLevel));
			result.append("</dataType>");
		}

		return result.toString();
	}
}
