package elements.dataelements;

import elements.Element;
import elements.ElementTag;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

/**
 * Created by Krzysztof S. on 2017-09-01.
 */
public abstract class ElementWithData extends Element {

	protected HBox leftSideDataLayout;

	protected HBox rightSideDataLayout;

	public ElementWithData(ElementTag elementTag, String elementName) {
		super(elementTag, elementName);

		layout = new HBox();
		labelName = new Label();
		labelName.setPrefHeight(25);
		labelName.setAlignment(Pos.BASELINE_RIGHT);
		labelName.setPadding(new Insets(9, 0, 0, 0));
		labelName.setMinWidth(150);
		labelName.setMaxWidth(150);

		leftSideDataLayout = new HBox();
		leftSideDataLayout.setPadding(new Insets(0, 0, 0, 10));

		rightSideDataLayout = new HBox();
		rightSideDataLayout.setPadding(new Insets(5, 10, 5, 5));
		rightSideDataLayout.setSpacing(5);

//		setUpPopOverNotification();
	}

	@Override
	public Pane refreshLayout() {
		layout.getChildren().clear();
		layout.getChildren().add(leftSideDataLayout);
		layout.getChildren().add(rightSideDataLayout);

		for (Element component: components)
			layout.getChildren().add(component.getFullLayout());

		return layout;
	}

	@Override
	public void setUpVariableName(String newName){
		this.name = newName;

		if(!("".equals(name))) {
			labelName.setText(name + ": ");
			getTreeItemHandler().setValue(newName);
		}
	}
}
