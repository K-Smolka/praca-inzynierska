package elements.dataelements;

import elements.Element;
import elements.ElementTag;
import elements.dataelements.simpledataelements.*;
import javafx.geometry.Insets;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import treeviewcustom.treeitems.AbstractTreeItem;
import treeviewcustom.treeitems.TreeItemsBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Krzysztof S. on 2017-09-01.
 */
public class Group extends ElementWithData{
	public ElementWithSimpleData patternToGroup;

	public Group(String elementName, ElementWithSimpleData patternToGroup, int listSize) {
		super(ElementTag.GROUP, elementName);
		setUpVariableName(elementName);
		this.patternToGroup = patternToGroup;

		layout = new VBox();

		labelName.setPadding(new Insets(0, 0, 0, 0));
		leftSideDataLayout.getChildren().add(labelName);

		for(int i=0; i<listSize; i++)
			components.add(cloneDataType(patternToGroup, "#" + (i+1)));

		treeItemHandler = getTreeItemHandler();
		refreshLayout();
	}

	public Group(Group toCopy){
		this(toCopy.getName(), toCopy.patternToGroup, toCopy.components.size());
	}

	public void setNewPattern(ElementWithSimpleData newPattern){
		this.patternToGroup = newPattern;
		int listSize = components.size();
		components.clear();

		for(int i=0; i<listSize; i++)
			components.add(cloneDataType(patternToGroup, "#" + (i+1)));

		refreshLayout();
	}

	public void setNewListSize(int newGroupSize){
		int oldGroupSize = components.size();
		if(newGroupSize > oldGroupSize)
			for(int i=0; i<(newGroupSize-oldGroupSize); i++)
				components.add(cloneDataType(patternToGroup, "#" + (i+1)));
		else if(newGroupSize < oldGroupSize)
			components = components.subList(0, newGroupSize - 1);
	}

	private ElementWithSimpleData cloneDataType(ElementWithSimpleData patternToGroup, String variableName){
		ElementWithSimpleData result;
		switch(patternToGroup.elementTag){
			case NUMERIC_VALUE:
				result = new NumericValue((NumericValue) patternToGroup);
				break;
			case STRING_VARIABLE:
				result = new StringVariable((StringVariable) patternToGroup);
				break;
			case POINT2D:
				result = new Point2d((Point2d) patternToGroup);
				break;
			case POINT3D:
				result = new Point3d((Point3d) patternToGroup);
				break;
			default:
				throw new IllegalArgumentException("Type is not groupable!");
		}

		result.setUpVariableName(variableName);
		return result;
	}

	@Override
	public Pane refreshLayout(){
		leftSideDataLayout.getChildren().clear();
		leftSideDataLayout.getChildren().add(labelName);

		layout.getChildren().clear();
		layout.getChildren().add(leftSideDataLayout);

		for (Element component: components)
			layout.getChildren().add(component.getFullLayout());

		return layout;
	}

	public void addSignleRow(){
		Element toAdd = cloneDataType(patternToGroup, "#" + (components.size()+1));
		this.components.add(toAdd);
		refreshLayout();
	}

	@Override
	protected boolean validateComponentTag(Element newComponent) {
		return false;
	}

	@Override
	public String getDataString(int tabLevel) {
		String result = getTabs(tabLevel) + this.getName() + " = (";

		List<String> componentsDataFieldsContent = new ArrayList<>();

		for (Element component : components)
			componentsDataFieldsContent.add("(" + ((ElementWithSimpleData) component).getUnwrappedDataString() + ")");

		result += String.join(", ", componentsDataFieldsContent) + ");";

		return result;
	}

	@Override
	public String convertToXML(int tabLevel){
		StringBuilder result = new StringBuilder();

		result.append(System.lineSeparator() + getTabs(tabLevel));

		if(components.isEmpty()){
			result.append("<dataType name=\"" + getName() + "\" type=\"" + this.elementTag + "\" length=\"0\" />");
		}else{
			result.append("<dataType name=\"" + getName() + "\" type=\"" + this.elementTag + "\" length=\"" +
					components.size() + "\" >");

			result.append(components.get(0).convertToXML(tabLevel+1));

			result.append(System.lineSeparator() + getTabs(tabLevel));
			result.append("</dataType>");
		}

		return result.toString();
	}

	@Override
	public AbstractTreeItem getTreeItemHandler(){
		if(treeItemHandler == null){
			treeItemHandler = TreeItemsBuilder.createTreeItem(this.getName(), this.ID, ElementTag.GROUP);
			for(Element component: components)
				treeItemHandler.getChildren().add(component.getTreeItemHandler());
		}

		return treeItemHandler;
	}

	// Obsluga dodawania wierszy do TreeItemu grupy:
//	private void refreshTreeItemHandler(){
//		treeItemHandler.getChildren().clear();
//
//		for(Element component: components)
//			treeItemHandler.getChildren().add(component.getTreeItemHandler());
//	}
}
