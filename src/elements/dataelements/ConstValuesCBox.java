package elements.dataelements;

import elements.Element;
import elements.ElementTag;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.Pane;

import java.util.List;

/**
 * Created by Krzysztof S. on 2017-09-01.
 */
public class ConstValuesCBox extends ElementWithData {

	/**
	 * consts - przechowuje wartosci obslugiwane przez ComboBox
	 */
	public ObservableList<String> consts;

	/**
	 * selectedConst - currently chosen constValue in ComboBox
	 */
	private String selectedConst;

	private ComboBox<String> comboBox;

	public ConstValuesCBox(String elementName, List<String> constsList) {
		super(ElementTag.CONST_VALUES_CBOX, elementName);
		setUpVariableName(elementName);

		consts = FXCollections.observableArrayList();

		if(constsList != null)
			consts.addAll(constsList);

		comboBox = new ComboBox(consts);
		comboBox.setMaxWidth(158);
		comboBox.valueProperty().addListener(new ChangeListener<String>() {
			@Override public void changed(ObservableValue observedValue, String oldValue, String newValue) {
				if(newValue != null)
					selectedConst = newValue;
				else
					selectedConst = "";
			}
		});

		if(!consts.isEmpty()){
			comboBox.setValue(consts.get(0));
			selectedConst = consts.get(0);
		}

		refreshLayout();
	}

	public ConstValuesCBox(ConstValuesCBox toCopy){
		this(toCopy.getName(), toCopy.consts);
	}

	public String getSelectedConst() {
		return selectedConst;
	}

	public void selectValue(String constValueToSelect){
		if(consts.contains(constValueToSelect))
			comboBox.setValue(consts.get(consts.indexOf(constValueToSelect)));	//??????? troche zwydziwiane
	}

	public void addNewConst(String newConst){
		if (newConst == null || "".equals(newConst))
			return;

		consts.add(newConst);
		comboBox.setValue(consts.get(0));
		refreshLayout();
	}

	@Override
	protected boolean validateComponentTag(Element newComponent) {
		return false;
	}

	@Override
	public String getDataString(int tabLevel) {
		String result;

		if(("").equals(selectedConst) || selectedConst == null)
			result = getTabs(tabLevel) + this.getName() + " = \"\";";
		else
			result = getTabs(tabLevel) + this.getName() + " = \"" + selectedConst + "\";";

		return result;
	}

	@Override
	public Pane refreshLayout(){
		layout.getChildren().clear();

		leftSideDataLayout.getChildren().clear();
		leftSideDataLayout.getChildren().add(labelName);
		layout.getChildren().add(leftSideDataLayout);

		rightSideDataLayout.getChildren().clear();
		rightSideDataLayout.getChildren().add(comboBox);
		layout.getChildren().add(rightSideDataLayout);

		return layout;
	}

	@Override
	public String convertToXML(int tabLevel){
		StringBuilder result = new StringBuilder();

		result.append(System.lineSeparator() + getTabs(tabLevel));

		if(consts.isEmpty()){
			result.append("<dataType type=\"" + this.elementTag + "\" " +  "name=\"" + getName() + "\" />");
		}else{
			result.append("<dataType type=\"" + this.elementTag + "\" " +  "name=\"" + getName() + "\" >");

			for(String s: consts){
				result.append(System.lineSeparator() + getTabs(tabLevel + 1));
				result.append("<constValue name=\"" + s + "\" />");
			}

			result.append(System.lineSeparator() + getTabs(tabLevel));
			result.append("</dataType>");
		}

		return result.toString();
	}
}
