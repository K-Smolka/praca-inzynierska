package elements;

import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import treeviewcustom.treeitems.AbstractTreeItem;
import treeviewcustom.treeitems.TreeItemsBuilder;
import utility.IDGenerator;
import validation.Validable;
import validation.ValidationResult;
import xmlconverter.ConvertibleToXML;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Krzysztof S. on 2017-09-01.
 */
public abstract class Element implements Validable, ConvertibleToXML{
	public final ElementTag elementTag;
	public final int ID;

	/**
	 * jest uzywana w TreeItemie i labelName.
	 */
	protected String name;

	/**
	 * Glowny layout danego DataType.
	 */
	protected Pane layout;

	protected Element parentHandler;

	/**
	 *
	 */
	public List<Element> components;

	/**
	 * Zawiera nazwe DataType. Jest wyswietlany po lewej stronie.
	 */
	protected Label labelName;

	/**
	 * Uchwyt do treeItema (w TreeView) który odpowiada temu DataType
	 */
	protected AbstractTreeItem treeItemHandler;


	public Element(ElementTag elementTag, String elementName) {
		this.elementTag = elementTag;
		this.ID = IDGenerator.getNewID();
		this.components = new ArrayList<>();
		this.name = elementName;
	}

	public Element(ElementTag elementTag) {
		this.elementTag = elementTag;
		this.ID = IDGenerator.getNewID();
		this.components = new ArrayList<>();
	}


	public Pane getFullLayout() {
		return layout;
	}

	public abstract Pane refreshLayout();

	protected Pane getAllComponentsPane(){
		Pane componentsPane = new VBox();
		for (Element component: components) {
			componentsPane.getChildren().add(component.getFullLayout());
		}
		return componentsPane;
	}

	/**
	 *
	 * @param newComponent
	 * @return returns true when component has been added correctly (type is ok.)
	 */
	public boolean addComponent(Element newComponent){
		if(!validateComponentTag(newComponent))
			return false;

		components.add(newComponent);
		getTreeItemHandler().getChildren().add(newComponent.getTreeItemHandler());

		refreshLayout();
		return true;
	}

	public boolean addAllComponents(List<? extends Element> newComponents){
		for(Element newComponent: newComponents)
			if(!validateComponentTag(newComponent))
				return false;

		for(Element newComponent: newComponents)
			addComponent(newComponent);

		return true;
	}

	/**
	 * is used in addComponent() method.
	 * @param newComponent
	 * @return
	 */
	protected abstract boolean validateComponentTag(Element newComponent);

	public void setUpVariableName(String newName){
		this.name = newName;

		if(!("".equals(name))) {
			labelName.setText(name);
			getTreeItemHandler().setValue(newName);
		}
	}

	public void setName(String newName) {
		name = newName;
		labelName.setText(name + ":");
	}

	public String getName(){
		return name;
	}

	/**
	 * Override this method if class got more complicated structure, for example, got subelements. It's used in Element constructor.
	 * @return
	 */
	public AbstractTreeItem getTreeItemHandler(){
		if(treeItemHandler == null)
			treeItemHandler = TreeItemsBuilder.createTreeItem(this.getName(), this.ID, this.elementTag);

		return treeItemHandler;
	}

	public Element searchAmongChildrensById(int id) {
		Element result = null;

		for(Element component: components)
			if(id == component.ID)
				return component;
			else
				if(component.searchAmongChildrensById(id) != null)
					result = component.searchAmongChildrensById(id);

		return result;
	}

	public boolean deleteChildrenComponentById(int id) {
		for(Element component: components)
			if(id == component.ID){
				components.remove(component);
				refreshLayout();
				return true;
			}
			else
				if(component.deleteChildrenComponentById(id) == true)
					return true;

		return false;
	}

	@Override
	public List<ValidationResult> validate() {
		List<ValidationResult> validationResultList = new ArrayList<>();

		for (Element element: components)
			validationResultList.addAll(element.validate());

		return validationResultList;
	}

	public abstract String getDataString(int tabLevel);

	protected String getTabs(int numberOfTabulators){
		String result = "";
		for(int i=0; i<numberOfTabulators; i++)
			result += "\t";

		return result;
	}
}
